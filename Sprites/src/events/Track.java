
package events;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;

public class Track extends Applet {
    MediaTracker t;
    Image i,j;
    
    public void init(){
        //System.out.println("path is: "+getCodeBase().getPath());
        
        setBackground(Color.black);
        t = new MediaTracker(this);
        i = getImage(getCodeBase(),"loading.gif");
        t.addImage(i, 0);
        j = getImage(getCodeBase(),"loading_j.gif");
        t.addImage(j, 0);
        showStatus("loading");
        
        try{
            t.waitForAll();
        } catch(Exception e){}
        
        if(t.isErrorAny()){
            showStatus("error");
        } else if(t.checkAll()){
            showStatus("successfully loaded");
        }
    }
    
    public void paint(Graphics g){
        g.drawImage(i, 13, 17, this);
        g.drawImage(j, 203, 207, this);
    }
    
}
