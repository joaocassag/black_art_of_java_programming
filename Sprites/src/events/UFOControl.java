
package events;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import sprites.BitmapSprite;
import sprites.Moveable;
import sprites.Sprite;

public class UFOControl extends Applet implements Runnable {
    
    Thread animation;
    Graphics sc;
    Image image;
    
    static final int NUM_SPRITES = 1;
    static final int REFRESH_RATE = 80;
    
    BitmapSprite sprites[];
    int width, height;
    
    public void init(){
        System.out.println(">> init <<");
        setBackground(Color.black);
        width = getBounds().width;
        height = getBounds().height;
        initSprites();
        image = createImage(width, height);
        sc = image.getGraphics();
    }

    @Override
    public void run() {
        while(true){
            repaint();
            updateSprites();
            try{
                Thread.sleep(REFRESH_RATE);
            }catch(Exception e){}
        }
    }

    private void initSprites() {
        sprites = new BitmapSprite[NUM_SPRITES];
        Image backImage;
        Image foreImage[] = new Image[6];
        
        MediaTracker t = new MediaTracker(this);
        backImage = getImage(getCodeBase(),"loading.gif");
        t.addImage(backImage,0);
        for(int i =0; i<6;i++){
            foreImage[i] = getImage(getCodeBase(),"loading.gif");
            t.addImage(foreImage[i], 0);
        }
        
        System.out.println("loading images");
        try{
            t.waitForAll();
        }catch(Exception e){return;}
        
        if(t.isErrorAny()){
            System.out.println("error");
        } else if (t.checkAll()){
            System.out.println("successfully loaded");
        }
        sprites[0] = new BitmapLoop(13,17,backImage,foreImage,this);
        
    }
    
    public boolean keyDown(Event e, int key){
        switch(key){
            case Event.RIGHT:
                ((Moveable)sprites[0]).setVelocity(3, 0);
                break;
            case Event.LEFT:
                ((Moveable)sprites[0]).setVelocity(-3, 0);
                break;
            case Event.UP:
                ((Moveable)sprites[0]).setVelocity(0, -3);
                break;
            case Event.DOWN:
                ((Moveable)sprites[0]).setVelocity(0, 3);
                break;
            default:
                break;
        }
        return true;
    }
    
    public void start(){
        System.out.println(">> start <<");
        animation = new Thread(this);
        if(animation !=null){
            animation.start();
        }
    }
    
    public void stop(){
        System.out.println(">> stop <<");
        if(animation !=null){
            animation.stop();
            animation = null;
        }
    }
    
    public void updateSprites(){
        for(int i =0; i<sprites.length;i++){
            sprites[i].update();
        }
    }
    
    public void update(Graphics g){
        paint(g);
    }
    
    public void paint(Graphics g){
        sc.setColor(Color.black);
        sc.fillRect(0, 0, width, height);
        
        for(int i=0; i<sprites.length;i++){
            sprites[i].paint(sc);
        }
        g.drawImage(image,0, 0, this);
    }
    
}
