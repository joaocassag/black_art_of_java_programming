
package events;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import sprites.BitmapSprite;
import sprites.Moveable;

public class BitmapLoop extends BitmapSprite implements Moveable {
    protected Image images[];
    protected int currentImage;
    protected boolean foreground;
    protected boolean background;
    
    protected int vx, vy;

    public BitmapLoop(int x, int y, Image b, Image f[], Applet a) {
        super(x, y, b, a);
        //width = image.getWidth(a);
        //height = image.getHeight(a);
        
        if(image!=null) background = true;
        else background =false;
        
        
        images = f;
        currentImage = 0;
        if(images==null || images.length ==0) foreground = false;
        else {
            foreground = true;
            if(!background){
                width = images[0].getWidth(a);
                height = images[0].getHeight(a);
            }
        }
    }
    
    public void update(){
        if(active && foreground){
            currentImage = (currentImage+1)%images.length;
        }
        updatePosition();
    }
    
    public void paint(Graphics g){
        if(visible){
            if(background)
                g.drawImage(image, locx, locy, applet);
            if(foreground)
                g.drawImage(images[currentImage], locx, locy, applet);
        }
    }

    @Override
    public void setPosition(int x, int y) {
        locx = x;
        locy = y;
    }

    @Override
    public void setVelocity(int x, int y) {
        vx = x;
        vy = y;
    }

    @Override
    public void updatePosition() {
        locx += vx;
        locy += vy;
        //vx = 0;
        //vy = 0;
    }
    
}
