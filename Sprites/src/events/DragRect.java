
package events;

import java.awt.Color;
import java.awt.Graphics;
import sprites.RectSprite;

public class DragRect extends RectSprite {
    
    protected boolean draggable;
    
    public DragRect(int x, int y, int w, int h, Color c) {
        super(x, y, w, h, c);
        fill = true;
        draggable = false;
    }
    
    public void setDraggable(boolean d){
        draggable = d;
    }
    
    public boolean isDraggable(){
        return draggable;
    }
    
    public boolean inside(int x, int y){
        return (locx<=x && locy<=y &&
                (locx+width>=x) &&
                (locy+height>=y));
    }
    
    public void translate(int x, int y){
        locx+=x;
        locy+=y;
    }
    
    public void grow(){
        width++;
        height++;
    }
    
    public void shrink(){
        if(width>0) width--;
        if(height>0) height--;
    }
    
    public void paint(Graphics g){
        g.setColor(Color.red);
        g.fillRect(locx, locy, width, height);
    }
    
}
