
package events;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class Drag extends Applet {
    Font courierFont;
    String testString = "Drag the Rectangle";
    DragRect r = new DragRect(0,0,107,103,Color.red);
    
    int oldx, oldy;
    
    public void init(){
        courierFont = new Font("Courier",Font.BOLD+Font.ITALIC,14);
    }
    
    public void paint(Graphics g){
        g.setFont(courierFont);
        
        FontMetrics m = g.getFontMetrics();
        int stringWidth = m.stringWidth(testString);
        int width = (getBounds().width- stringWidth)/2;
        int height = getBounds().height/2;
        
        g.setColor(Color.green);
        g.drawString(testString, width, height);
        r.paint(g);
    }
    
    public boolean mouseDown(Event e, int x, int y){
        if(r.inside(x, y)){
            oldx = x;
            oldy = y;
            r.setDraggable(true);
        }
        showStatus("down at ("+x+","+y+")");
        return true;
    }
    
    public boolean mouseUp(Event e, int x, int y){
        showStatus("up at ("+x+","+y+")");
        r.setDraggable(false);
        return true;
    }
    
    public boolean mouseDrag(Event e, int x, int y){
        showStatus("drag at ("+x+","+y+")");
        if(r.isDraggable()){
            r.translate(x-oldx, y-oldy);
            oldx = x;
            oldy = y;
            repaint();
        }
        return true;
    }
    
    public boolean keyDown(Event e, int key){
        showStatus("key down== "+(char)key+"("+key+")");
        switch(key){
            case Event.RIGHT:
                r.grow();
                repaint();
                break;
            case Event.LEFT:
                r.shrink();
                repaint();
                break;
            default:
                break;
        }
        return true;
    }
    
    
}
