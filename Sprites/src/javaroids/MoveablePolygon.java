
package javaroids;

import java.awt.Color;
import sprites.Moveable;

public class MoveablePolygon extends PolygonSprite implements Moveable {
    
    protected int tx[], ty[];
    protected int vx, vy;
    protected int height = 1000,
                  width =1000;
    protected int max_radius = 0;
    
    public MoveablePolygon(int[] tx, int[] ty, int n, int centerx, int centery, Color c) {
        super(tx, ty, n, centerx, centery, c);
        vx = vy = 0;
        this.tx = new int[n];
        this.ty = new int[n];
        for(int i=0;i<n;i++){
            this.tx[i] = tx[i];
            this.ty[i] = ty[i];
        }
    }
    
    public MoveablePolygon(int[] tx, int[] ty, int n, int centerx, int centery, Color c, int w, int h, int r) {
        super(tx, ty, n, centerx, centery, c);
        vx = vy = 0;
        this.tx = new int[n];
        this.ty = new int[n];
        for(int i=0;i<n;i++){
            this.tx[i] = tx[i];
            this.ty[i] = ty[i];
        }
        height = h;
        width = w;
        max_radius = r;
    }

    @Override
    public void setPosition(int x, int y) {
        locx = x;
        locy = y;
        
        for(int i =0; i<p.npoints;i++){
            p.xpoints[i]= locx+tx[i];
            p.ypoints[i]= locy+ty[i];
        }
    }

    @Override
    public void setVelocity(int x, int y) {
        vx = x;
        vy = y;
    }
    
    public void scale(double factor){
        for(int i =0; i<p.npoints;i++){
            tx[i] = (int)Math.round(factor*tx[i]);
            ty[i] = (int)Math.round(factor*ty[i]);
        }
    }

    @Override
    public void updatePosition() {
        locx +=vx;
        locy +=vy;
        
        if(locx-max_radius > width){
            locx -=width + 2*max_radius;
        } else if (locx < -max_radius){
            locx+= width +2*max_radius;
        }
        
        if(locy-max_radius > height){
            locy -=height + 2*max_radius;
        } else if (locy < -max_radius){
            locy+= height +2*max_radius;
        }
    }
    
    public void updatePoints(){
        for(int i =0; i<p.npoints;i++){
            p.xpoints[i] = locx+tx[i];
            p.ypoints[i] = locy+ty[i];
        }
    }
    
    public void udpate(){
        updatePosition();
        updatePoints();
    }
    
}
