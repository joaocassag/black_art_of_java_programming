
package javaroids;

import java.awt.Color;

public class Ship extends RotateablePolygon {
    static final int ROT_RATE=15;
    
    boolean rotated = false;
    
    int rotation_amount;
    
    static final double ACCEL_FACTOR=1.0;

    public Ship(int[] tx, int[] ty, int n, int centerx, int centery, Color c, int w, int h, int r) {
        super(tx, ty, n, centerx, centery, c, w, h, r);
        
        for(int i=0;i<angle.length;i++){
            angle[i] = checkAngleBounds(angle[i]-90);
        }
        rotate(-90);
    }
    
    public void rotateLeft(){
        rotated = true;
        rotation_amount = -ROT_RATE;
    }
    
    public void rotateRight(){
        rotated = true;
        rotation_amount = ROT_RATE;
    }
    
    public void update(){
        if(isActive()){
            updatePosition();
            
            if(rotated){
                rotate(rotation_amount);
                rotated=false;
            } else{
                updatePoints();
            }
        }
    }
    
    public void thrust(){
        vx= (int)Math.round((double)vx+GameMath.cos(theta)*ACCEL_FACTOR);
        vy= (int)Math.round((double)vy+GameMath.sin(theta)*ACCEL_FACTOR);
    }
    
}
