
package javaroids;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import sprites.Sprite2D;

public class PolygonSprite extends Sprite2D {
    
    protected Polygon p;
    
    public PolygonSprite(int x[], int y[], int n, Color c){
        p = new Polygon(x,y,n);
        setColor(c);
        visible = true;
        fill = false;
        locx = locy = 0;
    }
    
    public PolygonSprite(int tx[], int ty[], int n, 
                        int centerx, int centery, Color c){
        
        int x[] = new int[n], y[]= new int[n];
        for(int i =0;i<n;i++){
            x[i] = centerx +tx[i];
            y[i] = centery +ty[i];
        }
        
        p = new Polygon(x,y,n);
        locx = centerx;
        locy = centery;
        setColor(c);
        visible = true;
        fill = false;
    }
    
    public void addPoint(int x, int y){
        p.addPoint(x, y);
    }
    
    public void paint(Graphics g) {
        if(visible){
            g.setColor(getColor());
            if(fill)
                g.fillPolygon(p);
            else
                g.drawPolygon(p);
        }
    }
    
    public void update() {
        
    }
    
}
