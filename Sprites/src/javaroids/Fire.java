
package javaroids;

import java.awt.Color;

public class Fire extends MoveablePolygon {
    static final int firex[] ={0,0};
    static final int firey[] ={0,0};
    
    static final int DEFAULT_LENGH = 23;
    int fire_length = DEFAULT_LENGH;
    
    int max_updates = 14;
    
    int count =0;
    
    public Fire(Color c, int w, int h){
        super(firex,firey,2,0,0,c,w,h,DEFAULT_LENGH);
        setFill(false);
    }
    
    public Fire(Color c, int w, int h, int length, int updates){
        super(firex,firey,2,0,0,c,w,h,length);
        max_updates = updates;
        setFill(false);
    }
    
    public void initialize(int x, int y, int angle){
        tx[1]=(int)Math.round(fire_length*GameMath.cos(angle));
        ty[1]=(int)Math.round(fire_length*GameMath.sin(angle));
        setPosition(x,y);
        count=0;
        restore();
    }
    
    public void update(){
        if(isActive()){
            count++;
            if(count==max_updates){
                suspend();
            } else super.update();
        }
    }
    
    public boolean interserct(Ship s){
        return isActive() && s.isActive() &&
                (Math.abs(s.locx-locx+2)<max_radius) &&
                    (Math.abs(s.locy-locy+2)<max_radius);
    
    }
    
}
