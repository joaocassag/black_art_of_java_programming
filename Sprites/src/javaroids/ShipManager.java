
package javaroids;

import java.awt.Color;
import java.awt.Event;
import java.awt.Graphics;

public class ShipManager {
    //coordinates of the ship sprite polyogn
    static final int tpx[]={0,-7,7,0};
    static final int tpy[]={13,-7,7,13};
    
    // 5 shots at a time
    static final int MAX_SHOTS=5;
    
    //lenght of ship's nose
    static final int SHIP_LENGHT=13;
    
    static int width, height;
    private int shipsLeft;
    private boolean shipAlive;
    private boolean shieldOn;
    private int shieldStrength;
    private boolean gameOver;
    
    private Ship s;
    private Fire f[] = new Fire[MAX_SHOTS];
    EffManager efm;
    GameManager game;
    
    public ShipManager(int width, int height, EffManager efm, GameManager game ){
        s = new Ship(tpx, tpy, tpx.length, width/2, height/2, Color.yellow, width, height, SHIP_LENGHT);
        
        for(int i=0; i<MAX_SHOTS;i++){
            f[i] = new Fire(Color.white, width, height);
            f[i].suspend();
        }
        
        this.efm = efm;
        this.game = game;
        this.width = width;
        this.height = height;
        shipsLeft = 2;
        shipAlive = true;
        shieldOn = false;
        shieldStrength = 100;
        gameOver  = false;
        
        s.restore();
    }
    
    public Ship getShip(){
        return s;
    }
    
    public Fire[] getFire(){
        return f;
    }
    
    public void stopFire(int i){
        f[i].suspend();
    }
    public void extraShip(){
        shipsLeft++;
    }
    
    static int waitPeriod;
    
    public void destroyShip(){
        if(gameOver) return;
        
        if(!(keyState[SHIELDINDEX] && shieldStrength>0)){
            shipAlive = false;
            s.suspend();
            shipsLeft--;
            if(shipsLeft<0){
                game.setGameOver();
                gameOver = true;
            }
            waitPeriod = 50;
            clearKeyState();
            
            efm.addShipExplosion(s.locx, s.locy);
        }
    }
    
    private void clearKeyState(){
            for(int i =0;i<keyState.length;i++){
                keyState[i]=false;
            }
    }
    
    //pad  number with zeroes, up to given length l
    
    public String PadNumber(int n, int l){
        StringBuffer s = new StringBuffer(Integer.toString(n));
        while(s.length()<l){
            s.insert(0, "0");
        }
        return s.toString();
    }
    
    String shieldString = "Shild:";
    String noShieldString = "No Shiled";
    String shipsLeftString="Ships Left:";
    
    public void paint(Graphics g){
        g.setColor(Color.yellow);
        
        //draw ships left indidcator
        g.drawString(shipsLeftString, 69, 20);
        g.drawString(PadNumber(shipsLeft,1), 135, 20);
        
        //if ship's alive
        if(shipAlive){
            //if shield's on
            if(keyState[SHIELDINDEX] && shieldStrength>0){
                g.setColor(Color.green);
                g.drawOval(s.locx-15, s.locy-15, 30, 30);
                g.drawOval(s.locx-17, s.locy-17, 34, 34);
            }
            
            //draw shield status
            if(shieldStrength>0){
                g.setColor(Color.green);
                g.drawString(shieldString, 153, 20);
                g.fillRect(199, 15, shieldStrength, 5);
            } else{
                g.setColor(Color.red);
                g.drawString(noShieldString, 153, 20);
            }
            
            s.paint(g);
            
            for(int i=0;i<MAX_SHOTS;i++){
                f[i].paint(g);
            }
        }
    }
    static boolean keyState[] = new boolean[5];
    
    //key bindings
    static final int LEFTINDEX=0;
    static final int LEFT = 'q';
    
    static final int RIGHTINDEX=1;
    static final int RIGHT = 'w';
    
    static final int THRUSTINDEX=2;
    static final int THRUST = 'o';
    
    static final int FIREINDEX=3;
    static final int FIRE = 'p';
    
    static final int SHIELDINDEX=4;
    static final int SHIELD = ' ';
    
    public void upate(){
        for(int i =0; i<MAX_SHOTS;i++){
            f[i].update();
        }
        
        if(shipAlive){
            if(keyState[LEFTINDEX])
                s.rotateLeft();
            if(keyState[RIGHTINDEX])
                s.rotateRight();
            if(keyState[THRUSTINDEX])
                s.thrust();
            if(keyState[FIREINDEX])
                handleFire();
            if(keyState[SHIELDINDEX])
                updateShield();
            
            s.update();
        } else{
            if(!gameOver && (waitPeriod-- <=0)){
                s.setAngle(270);
                s.setPosition(width/2, height/2);
                s.setVelocity(0,0);
                s.rotate(0);
                shieldStrength = 100;
                shipAlive = true;
                s.restore();
            }
        }
    }
    
    public void newGame(){
        s.setAngle(270);
        s.setPosition(width/2, height/2);
        s.setVelocity(0,0);
        s.rotate(0);
        shieldStrength = 100;
        shipAlive = true;
        shipsLeft=2;
        gameOver = false;
        s.restore();
    }
    
    public void increaseShield(){
        shieldStrength +=30;
        if(shieldStrength>250)
            shieldStrength=250;
    }
    
    public void keyDown(Event e, int k){
        switch(k){
            case LEFT:
                keyState[LEFTINDEX]=true;
                break;
            case RIGHT:
                keyState[RIGHTINDEX]=true;
                break;
            case FIRE:
                keyState[FIREINDEX]=true;
                break;
            case THRUST:
                keyState[THRUSTINDEX]=true;
                break;
            case SHIELD:
                keyState[SHIELDINDEX]=true;
                break;
            default:
                break;
        }
    }
    
    public void keyUp(Event e, int k){
        switch(k){
            case LEFT:
                keyState[LEFTINDEX]=false;
                break;
            case RIGHT:
                keyState[RIGHTINDEX]=false;
                break;
            case FIRE:
                keyState[FIREINDEX]=false;
                break;
            case THRUST:
                keyState[THRUSTINDEX]=false;
                break;
            case SHIELD:
                keyState[SHIELDINDEX]=false;
                break;
            default:
                break;
        }
    }
    
    private void updateShield(){
        if(shieldStrength--<=0){
            shieldStrength=0;
        }
    }
    
    private void handleFire(){
        for(int i=0;i<MAX_SHOTS;i++){
            if(f[i].isActive()){
                f[i].initialize(s.p.xpoints[0], s.p.ypoints[0], s.theta);
                break;
            }
        }
    }
    
    
}
