
package javaroids;

import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Enumeration;
import java.util.Vector;
import sprites.Sprite;

public class EffManager {
    
    Vector explosions;
    AudioClip expsound;
    
    public EffManager(AudioClip expsound){
        explosions = new Vector();
        this.expsound = expsound;
    }
    
    public void addShipExplosion(int x, int y){
        Explosion exp = new Explosion(Color.red,x,y,20);
        Explosion exp1 = new Explosion(Color.yellow,x+2,y+2,25);
        explosions.addElement(exp);
        explosions.addElement(exp1);
        if(expsound!=null){
            expsound.play();
        }
    }
    
    public void addAstExplosion(int x, int y){
        Explosion exp = new Explosion(Color.white,x,y,15);
        explosions.addElement(exp);
        if(expsound!=null){
            expsound.play();
        }
    }
    
    public void addEnemyExplosion(int x, int y){
        Explosion exp = new Explosion(Color.orange,x,y,15);
        explosions.addElement(exp);
        if(expsound!=null){
            expsound.play();
        }
    }
    
    public void addExplosion(Color c,int x, int y, int u){
        Explosion exp = new Explosion(c,x,y,u);
        explosions.addElement(exp);
        if(expsound!=null){
            expsound.play();
        }
    }
    
    public void paint(Graphics g){
        Enumeration e = explosions.elements();
        while(e.hasMoreElements()){
            Sprite exp = (Sprite)e.nextElement();
            exp.paint(g);
        }
    }
    
    public void update(){
        Enumeration e = explosions.elements();
        while(e.hasMoreElements()){
            Explosion exp = (Explosion)e.nextElement();
            exp.update();
            if(exp.isDone()){
                explosions.removeElement(exp);
            }
        }
    }
}
