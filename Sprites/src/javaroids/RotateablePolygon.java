
package javaroids;

import java.awt.Color;

public class RotateablePolygon extends MoveablePolygon {
    
    protected float magnitude[];
    protected int theta = 0;
    
    protected int angle[];
    protected int rate = 0;

    public RotateablePolygon(int[] tx, int[] ty, int n, int centerx, int centery, Color c, int w, int h, int r) {
        super(tx, ty, n, centerx, centery, c, w, h, r);
        magnitude = new float[n];
        angle = new int[n];
        
        for(int i=0;i<n;i++){
            magnitude[i] = (float) GameMath.computeMagnitude(tx[i], ty[i]);
            angle[i] = (int)Math.round( GameMath.computeAngle(tx[i], ty[i]) ) ;
        }
    }
    public void setAngle(int a){
        theta = a;
    }
    
    public void setRotationRate(int r){
        rate = r;
    }
    
    public void updateAngle(){
        theta = checkAngleBounds(theta+rate);
    }
    
    public void updateAngle(int a){
        theta = checkAngleBounds(theta+a);
    }
    
    public int checkAngleBounds(int th){
        if(th >=360)
            th -=360;
        else if(th<0)
            th+=360;
        return th;
    }
    
    public void rotate(int a){
        theta = checkAngleBounds(theta+a);
        
        for(int i=0;i<p.npoints;i++){
            double tempx, tempy;
            tempx = magnitude[i]*GameMath.cos(theta);
            tempy = magnitude[i]*GameMath.sin(theta);
            tx[i]=(int)Math.round(tempx);
            p.xpoints[i] = (int)Math.round(tempx + (double)locx);
            ty[i]=(int)Math.round(tempy);
            p.ypoints[i] = (int)Math.round(tempy + (double)locy);
        }
    }
    
    public void update(){
        updatePosition();
        rotate(rate);
        updateAngle();
    }
    
}
