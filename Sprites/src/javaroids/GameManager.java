
package javaroids;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Event;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;
import java.util.Date;

public class GameManager extends Applet implements Runnable {
    
    Thread animation;
    Graphics gbuf;
    Image im;
    static final int REFRESH_RATE=72;
    
    //size of the game applet
    static final int MAX_HEIGHT = 400;
    static final int MAX_WIDTH = 400;
    
    //game parameters
    private int score;
    private boolean playing;
    private boolean gameOver;
    private int intro_screen;
    static final int EXTRA_SHIP=10_000;
    private int current_extra=0;
    
    //constants for the introduction part of the game
    static final int INSTRUCTIONS = 1;
    static final int TEST_DRIVE = 2;
    static final int INTRO_OVER = 3;
    
    //references to manager classes
    AstManager am;
    ShipManager sm;
    EnemyManager em;
    EffManager efm;
    
    //sound
    URL codebase;
    AudioClip expsound = null;
    
    //variables to monirore performance during game
    Date time;
    long t1,t2,dt;
    
    //fonts
    static Font bigFont = new Font("TimesRoman", Font.BOLD,24);
    static Font medFont = new Font("TimesRoman", Font.BOLD,18);
    static Font smallFont = new Font("TimesRoman", Font.BOLD,12);
    
    public void init(){
        setBackground(Color.black);
        setFont(smallFont);
        
        im = createImage(MAX_WIDTH,MAX_HEIGHT);
        gbuf = im.getGraphics();
        
        codebase = getCodeBase();
        resize(MAX_WIDTH,MAX_HEIGHT);
        
        gameOver = true;
        intro_screen = 1;
        playing = false;
    }
    
    public void start(){
        try{
            expsound = getAudioClip(getCodeBase(),"Exlosion.au");
        }catch(Exception e){
            System.out.println("Sound not loaded");
        }
        
        efm = new EffManager(expsound);
        sm = new ShipManager(MAX_WIDTH,MAX_HEIGHT,efm, this);
        em = new EnemyManager(MAX_WIDTH,MAX_HEIGHT,sm,efm, this);
        am = new AstManager(MAX_WIDTH,MAX_HEIGHT,sm,em,efm, this);
        
        if(animation==null){
            animation = new Thread(this);
            if(animation!=null){
                animation.start();
            }
            else
                System.out.println("Insufficient memory to fork thread!");
        }
    }
    
    public void stop(){
        if(animation !=null){
            animation.stop();
            animation = null;
        }
    }
    
    public void setGameOver(){
        gameOver = true;
    }
    
    private void newGame(){
        em.newGame();
        am.newGame();
        sm.newGame();
        
        gbuf.setFont(smallFont);
        
        current_extra = 0;
        score = 0;
        gameOver = false;
        playing = true;
    }
    
    //Event handlers
    public boolean mouseUp(Event e, int x, int y){
        if(intro_screen == INSTRUCTIONS){
            intro_screen = TEST_DRIVE;
            sm.getShip().setPosition(385,75);
            sm.getShip().restore();
        } else if(intro_screen == TEST_DRIVE){
            intro_screen = INTRO_OVER;
            newGame();
        }
        else
            newGame();
        
        return true;
    }
    
    public boolean keyDown(Event e,int k){
        sm.keyDown(e, k);
        return true;
    }

    public boolean keyUp(Event e,int k){
        sm.keyUp(e, k);
        return true;
    }
    
    //the game driver
    
    @Override
    public void run() {
        startInstructions();
        
        while(true){
            if(playing){
                time = new Date();
                t1 = time.getTime();
                if(!gameOver){
                    sm.upate();
                }
                em.update();
                am.update();
                efm.update();
                repaint();
                
                Thread.currentThread().yield();
                time = new Date();
                t2 = time.getTime();
                dt = t2-t1;
                
                try{
                    Thread.sleep(REFRESH_RATE-(int)dt < 0 ? 8 : REFRESH_RATE-(int)dt);
                }catch(Exception e){}
            }
            else{
                updateOpening();
                repaint();
                Thread.currentThread().yield();
                try{
                    Thread.sleep(REFRESH_RATE);
                }catch(Exception e){}
            }
        }
    }
    
    private void startInstructions(){
        Asteroid a[] = am.getAsts();
        Asteroid p = am.getPowerup();
        //set asteroids in instruction screen
        a[0].setPosition(270,98);
        a[0].setRotationRate(4);
        a[0].setVelocity(1,2);
        a[0].restore();
        
        a[am.NUM_LARGE].setPosition(330,138);
        a[am.NUM_LARGE].setRotationRate(-4);
        a[am.NUM_LARGE].setVelocity(-1,-1);
        a[am.NUM_LARGE].restore();
        
        a[am.NUM_LARGE+am.NUM_MEDIUM].setPosition(370,178);
        a[am.NUM_LARGE+am.NUM_MEDIUM].setRotationRate(5);
        a[am.NUM_LARGE+am.NUM_MEDIUM].setVelocity(2,-1);
        a[am.NUM_LARGE+am.NUM_MEDIUM].restore();
        
        //set powerup in intro screen
        p.setPosition(330,340);
        p.restore();
        
        //set enemies for intro screen
        Enemy e[] = em.getEnemy();
        e[0].setPosition(10,214);
        e[0].setVelocity(2,0);
        e[0].restore();
        
        e[1].setPosition(390,254);
        e[1].setVelocity(-3,0);
        e[1].restore();
        
        e[2].setPosition(7,294);
        e[2].setVelocity(4,0);
        e[2].restore();
    }
    
    public void updateOpening(){
        if(intro_screen == INSTRUCTIONS){
            am.update();
            Enemy e[] = em.getEnemy();
            e[0].udpate();
            e[1].udpate();
            e[2].udpate();
            efm.update();
        }else if(intro_screen == TEST_DRIVE){
            sm.upate();
        }
    }
    
    public void updateScore(int val){
        score +=val;
        if((score/EXTRA_SHIP)>current_extra){
            sm.extraShip();
            current_extra = score/EXTRA_SHIP;
        }
    }
    
    public void update(Graphics g){  
        paint(g);
    }
    
    public String PadNumber(int n, int len){
        StringBuffer s = new StringBuffer(Integer.toString(n));
        while(s.length()<len){
            s.insert(0, "0");
        }
        return s.toString();
    }
    
    //define  string used in the opening screen
    //instructions
    String javaroidsString = "JAVAroinds!";
    String galleryString = "The Lineup...";
    String largeAstString = "30 points for Big Asteroid";
    String mediumAstString = "60 points for Medium Asteroid";
    String smallAstString = "90 points for Small Asteroid";
    String greenAlienString = "500 points for Green Alien";
    String redAlienString = "750 points for Red Alien";
    String orangeAlienString = "1000 points for Orange Alien";
    String clickMouseString1 = "Click mouse to continue...";
    String powerupString1 = "Touch powerup for";
    String powerupString2 = "extra shield strengh:";
    
    //test_drive
    String shipControlString = "Test Drive for you ship NOW --->";
    String rotLeftString = "Press 'q' to Rotate Left";
    String rotRightString = "Press 'w' to Rotate Right";
    String thrustString = "Press 'o' to Thrust";
    String fireString = "Press 'p' to Fire";
    String shieldString = "Press <space bar> for Shield";
    
    String extraShipString = "Extra Ship every 10_000 points";
    String goodLuckString ="GOOD LUCK!";
    String clickMouseString = "Click mouse to begin!";
    String byeMeString="by Author";
    
    String gameOverString = "GAME OVER";
    String clickMouseString2="Click mouse to play again!";
    
    public void paint(Graphics g){
        gbuf.setColor(Color.black);
        gbuf.fillRect(0,0, MAX_WIDTH, MAX_HEIGHT);
        
        if(playing){
            if(!gameOver){
                sm.paint(gbuf);
            }else{
                gbuf.setFont(bigFont);
                gbuf.setColor(Color.cyan);
                gbuf.drawString(byeMeString, MAX_WIDTH/2-55, MAX_HEIGHT/2-60);
                
                gbuf.setColor(Color.magenta);
                gbuf.drawString(gameOverString, MAX_WIDTH/2-65, MAX_HEIGHT/2);
                gbuf.drawString(clickMouseString2, MAX_WIDTH/2-120, MAX_HEIGHT/2+35);
                
                gbuf.setFont(smallFont);
            }
            
            em.paint(gbuf);
            am.paint(gbuf);
            efm.paint(gbuf);
            
            gbuf.drawString(PadNumber(score,6),20,20);
            
            g.drawImage(im,0,0, this);
        }
        else if(intro_screen == INSTRUCTIONS){
            paintInstructions(g);
        }
        else if(intro_screen == TEST_DRIVE){
            paintTestDrive(g);
        }
    }
    
    public void paintInstructions(Graphics g){
        gbuf.setFont(bigFont);
        gbuf.setColor(new Color(GameMath.getRand(155)+100,
                                GameMath.getRand(155)+100,
                                GameMath.getRand(155)+100));
        gbuf.drawString(javaroidsString, MAX_WIDTH/2-60,30);
        gbuf.setColor(Color.yellow);
        gbuf.setFont(medFont);
        gbuf.setColor(Color.magenta);
        gbuf.setFont(bigFont);
        gbuf.drawString(largeAstString,25,80);
        gbuf.drawString(mediumAstString,25,120);
        gbuf.drawString(smallAstString,25,160);
        gbuf.setColor(Color.green);
        gbuf.drawString(greenAlienString,25,200);
        gbuf.setColor(Color.red);
        gbuf.drawString(redAlienString,25,240);
        gbuf.setColor(Color.orange);
        gbuf.drawString(orangeAlienString,25,280);
        gbuf.setColor(Color.green);
        gbuf.drawString(orangeAlienString,25,280);
        gbuf.drawString(powerupString1,25,320);
        gbuf.drawString(powerupString2,50,350);
        gbuf.setColor(Color.yellow);
        gbuf.drawString(clickMouseString1,MAX_WIDTH/2-120,385);
        am.paint(g);
        Enemy e[] = em.getEnemy();
        e[0].paint(g);
        e[1].paint(g);
        e[2].paint(g);
        efm.paint(g);
        // dump offscrreen buffer to the screen
        g.drawImage(im,0,0,this);
    }
    
    public void paintTestDrive(Graphics g){
        gbuf.setFont(bigFont);
        gbuf.setColor(new Color(GameMath.getRand(155)+100,
                                GameMath.getRand(155)+100,
                                GameMath.getRand(155)+100));
        gbuf.drawString(javaroidsString, MAX_WIDTH/2-60,50);
        gbuf.setColor(Color.magenta);
        gbuf.drawString(shipControlString,25,80);
        gbuf.setColor(Color.orange);
        gbuf.drawString(rotLeftString,25,130);
        gbuf.drawString(rotRightString,25,170);
        gbuf.drawString(thrustString,25,250);
        gbuf.drawString(fireString,25,290);
        gbuf.drawString(shieldString,25,170);
        gbuf.setColor(Color.cyan);
        gbuf.drawString(extraShipString,25,340);
        gbuf.setColor(Color.green);
        gbuf.drawString(clickMouseString,MAX_WIDTH/2-120,385);
        g.drawImage(im,0,0,this);
    }
}
