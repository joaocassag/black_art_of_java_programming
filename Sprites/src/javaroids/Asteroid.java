
package javaroids;

import java.awt.Color;

public class Asteroid extends RotateablePolygon {
    static final int MAX_ROTATE_RATE=8;
    
    public int value;
    
    public Asteroid(int tx[], int ty[], int n,
                     int centerx, int centery, Color c,
                     int w, int h, int r, int v){
        super(tx, ty, n, centerx, centery, c, w, h, r);
        value = v;
        rate = GameMath.getRand(2*MAX_ROTATE_RATE)-MAX_ROTATE_RATE;
        setFill(true);
    }
    
    public boolean interserct(Fire f){
        if(isActive() && f.isActive()){
            int midptx = (f.p.xpoints[1]+f.p.xpoints[0])/2;
            int midpty = (f.p.ypoints[1]+f.p.ypoints[0])/2;
            return (Math.abs(midptx-locx)<max_radius) &&
                    (Math.abs(midpty-locy)<max_radius);
        }else
            return false;
    
    }
    
    public boolean interserct(Ship s){
        return isActive() && s.isActive() &&
                (Math.abs(s.locx-locx+2)<max_radius) &&
                    (Math.abs(s.locy-locy+2)<max_radius);
    
    }
    
    public boolean interserct(Enemy e){
        return isActive() && e.isActive() &&
                (Math.abs(e.locx-locx+2)<max_radius) &&
                    (Math.abs(e.locy-locy+2)<max_radius);
    
    }
    
}
