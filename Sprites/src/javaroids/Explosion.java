
package javaroids;

import java.awt.Color;

public class Explosion extends ContainerSprite {
    protected int locx, locy;
    
    static final int EXP_SIZE = 13;
    
    protected int max_updates = 0;
    
    protected int updates;
    
    static final int ex[][] = {{0,0},{-EXP_SIZE,EXP_SIZE},
                                {0,0},{-EXP_SIZE,EXP_SIZE}};
    static final int ey[][] = {{-EXP_SIZE,EXP_SIZE},{0,0},
                                {-EXP_SIZE,EXP_SIZE},{0,0}};
    
    MoveablePolygon edges[] = new MoveablePolygon[ex.length];
    
    protected float size;
    
    protected boolean done = false;
    
    public Explosion(Color color, int x, int y, int u){
        for(int i =0; i<edges.length;i++){
            edges[i] = new MoveablePolygon(ex[i],ey[i],ex.length,x,y,color);
            edges[i].restore();
            addSprite(edges[i]);
        }
        max_updates = u;
        
        updates =  0;
        locx = x;
        locy = y;
        restore();
        
        size =1.0f;
        
        done = false;
    }
    
    public void update(){
        if(updates++ < max_updates){
            size *=1.7f;
            
            int isize = (int)size;
            edges[0].setPosition(locx+isize, locy);
            edges[1].setPosition(locx,locy+isize);
            edges[2].setPosition(locx-isize, locy);
            edges[3].setPosition(locx,locy-isize);
        } else
            done = true;
    }
    
    public boolean isDone(){
        return done;
    }
    
}
