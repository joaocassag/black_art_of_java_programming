
package javaroids;

import java.awt.Graphics;
import java.util.Enumeration;
import java.util.Vector;
import sprites.Sprite;

public class ContainerSprite extends Sprite {
    
    protected Vector sprites;
    
    public ContainerSprite(){
        sprites = new Vector();
    }
    
    public void addSprite(Sprite s){
        sprites.addElement(s);
    }

    @Override
    public void paint(Graphics g) {
        if(visible){
            Enumeration e = sprites.elements();
            while(e.hasMoreElements()){
                Sprite s = (Sprite) e.nextElement();
                s.paint(g);
            }
        }
    }

    @Override
    public void update() {
        if(isActive()){
            Enumeration e = sprites.elements();
            while(e.hasMoreElements()){
                Sprite s = (Sprite) e.nextElement();
                s.update();
            }
        
        }
    }
    
}
