
package javaroids;

import java.awt.Color;
import java.awt.Graphics;

public class EnemyManager {
    // -- constants to define two sizes of enemy ships
    static final int LARGE = 0;
    static final int SMALL = 1;
    static final int SRATX = 2;
    static final int SRATY = 3;
    
    static final int tpx[][]={
        {14,11,8,-8,-11,-14,-6,6,14,-14},
        {14/SRATX,11/SRATX,8/SRATX,-8/SRATX,-11/SRATX,-14/SRATX,-6/SRATX,6/SRATX,14/SRATX,-14/SRATX}
    };
    
    static final int tpy[][]={
        {0,3,6,6,3,0,-4,-4,0,0},
        {0/SRATY,3/SRATY,6/SRATY,6/SRATY,-3/SRATY,0/SRATY,-4/SRATY,-4/SRATY,0/SRATY,0/SRATY}
    };
    
    // --screen boundaries
    static int width, height;
    
    // constants parameters for enemy ships
    static final int MAX_ENEMIES = 3;
    static final int MAX_SHOTS_PER_ENEMY = 2;
    static final int MAX_SHOTS = MAX_ENEMIES*MAX_SHOTS_PER_ENEMY;
    
    static final Color ENEMY_COLOR[] = {Color.green, Color.red, Color.orange};
    static final int ENEMY_LENGTH[] = {14,14,5};
    static final int VALUE[] = {500,750,1000};
    
    //maximum speed
    static final int MAX_VX =9, MAX_VY =9;
    
    //how often enemies appear, and how often they fire
    static double enemy_freq = .995;
    static double fire_freq = .995;
    
    private Enemy e[] = new Enemy[MAX_ENEMIES];
    
    private Fire f[] = new Fire[MAX_SHOTS_PER_ENEMY*MAX_ENEMIES];
    
    //references to other game objects
    ShipManager sm;
    EffManager efm;
    Ship ship;
    Fire[] shipfire;
    GameManager game;
    
    public EnemyManager(int width, int height, ShipManager sm, EffManager efm, GameManager g){
        ship = sm.getShip();
        shipfire = sm.getFire();
        this.sm = sm;
        this.efm = efm;
        game = g;
        
        e[0] = new Enemy(tpx[0],tpy[0],tpx.length,0,0,ENEMY_COLOR[0],width,height,ENEMY_LENGTH[0],VALUE[0]);
        e[1] = new Enemy(tpx[0],tpy[0],tpx.length,0,0,ENEMY_COLOR[1],width,height,ENEMY_LENGTH[1],VALUE[1]);
        e[2] = new Enemy(tpx[1],tpy[1],tpx[1].length,0,0,ENEMY_COLOR[2],width,height,ENEMY_LENGTH[2],VALUE[2]);
        
        for(int i =0; i<MAX_ENEMIES;i++){
            e[i].suspend();
        }
        
        for(int i=0;i<MAX_SHOTS;i++){
            f[i] = new Fire(ENEMY_COLOR[i/MAX_SHOTS_PER_ENEMY],width,height);
            f[i].suspend();
        }
        
        this.width = width;
        this.height = height;
    }
    
    public void newGame(){
        for(int i =0; i<MAX_ENEMIES;i++){
            e[i].suspend();
        }
        
        for(int i=0;i<MAX_SHOTS;i++){
            f[i] = new Fire(ENEMY_COLOR[i/MAX_SHOTS_PER_ENEMY],width,height);
            f[i].suspend();
        }
    }
    
    //update enemy ships and enemy fire
    
    public void update(){
        for(int i=0;i<MAX_SHOTS;i++){
            f[i].update();
            if(f[i].interserct(ship)){
                sm.destroyShip();
            }
        }
        
        for(int i =0; i<MAX_ENEMIES;i++){
            if(!e[i].isActive()){
                if(Math.random()>enemy_freq){
                    NewEnemy(i);
                }
            } else{
                e[i].update();
                
                if(e[i].interserct(ship)){
                    game.updateScore(e[i].value);
                    sm.destroyShip();
                    e[i].suspend();
                    efm.addEnemyExplosion(e[i].locx,e[i].locy);
                }
                
                //check if enemy intersect ship's fire
                for(int j =0; j<shipfire.length;j++){
                    if(e[i].interserct(shipfire[j])){
                        sm.stopFire(j);
                        e[i].suspend();
                        efm.addEnemyExplosion(e[i].locx,e[i].locy);
                        game.updateScore(e[i].value);
                    }
                }
                
                //create new enemy fire at random intervals
                if(Math.random()>fire_freq){
                    Fire(i);
                }
            }
        }
    }
    
    public Enemy[] getEnemy(){
        return e;
    }
    
    public Fire[] getFire(){
        return f;
    }
    
    public void destroyEnemy(int i){
        e[i].suspend();
    }
    
    public void stopFire(int i){
        f[i].suspend();
    }
    
    private void NewEnemy(int i){
        e[i].setVelocity(GameMath.getRand(2*MAX_VX)-MAX_VX, GameMath.getRand(2*MAX_VY)-MAX_VY);
        int px = (Math.random()>0.5)? 0 : width;
        int py = GameMath.getRand(height);
        e[i].setPosition(px, py);
        e[i].restore();
    }
    
    private void Fire(int i){
        for(int j= i*MAX_SHOTS_PER_ENEMY; j<(i+1)*MAX_SHOTS_PER_ENEMY;j++){
            if(!f[j].isActive()){
                f[j].initialize(e[i].locx, e[i].locy, GameMath.getRand(360));
                f[j].restore();
            }
        }
    }
    
    public void paint(Graphics g){
        for(int i =0; i<MAX_ENEMIES;i++){
            e[i].paint(g);
        }
        
        for(int i =0; i<MAX_SHOTS;i++){
            f[i].paint(g);
        }
    }
    
}
