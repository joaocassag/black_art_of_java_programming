
package threedD;

public class fPoint3d {
    public double x, y, z;
    
    public fPoint3d(double x0, double y0, double z0){
        x=x0;
        y=y0;
        z=z0;
    }
    
    public fPoint3d(){
        x=y=z=0;
    }
    
    public boolean equals(fPoint3d p){
        return (p.x==x) && (p.y==y) && (p.z==z);
    }
    
    public void set(fPoint3d p){
        x=p.x;
        y=p.y;
        z=p.z;
    }
    
}
