
package threedD;

import java.io.InputStream;
import java.io.StreamTokenizer;

public class fArrayOf3dPoints {
    
    double x[],y[],z[];
    int npoints;
    
    public fArrayOf3dPoints(double x0[], double y0[], double z0[], int n){
        x = x0;
        y=y0;
        z=z0;
        npoints = n;
    }
    
    public fArrayOf3dPoints(int n){
        npoints = n;
        x = new double[n];
        y = new double[n];
        z = new double[n];
    
    }
    
    public fArrayOf3dPoints(InputStream is) throws Exception{
        fromString(is);
    }
    
    public String toString(){
        String str = " "+npoints+"\n";
        for(int n=0; n<npoints;n++){
            str = str+x[n]+" "+y[n]+" "+z[n]+"\n";
        }
        return str;
    }
    
    public fArrayOf3dPoints makeClone(){
        double xnew[], ynew[], znew[];
        System.arraycopy(x,0, xnew = new double[npoints], 0, npoints);
        System.arraycopy(y,0, ynew = new double[npoints], 0, npoints);
        System.arraycopy(z,0, znew = new double[npoints], 0, npoints);
        return new fArrayOf3dPoints(xnew,ynew,znew,npoints);
    }
    
    public void fromString(InputStream is) throws Exception{
        StreamTokenizer stream = new StreamTokenizer(is);
        stream.commentChar('#');

        stream.nextToken();
        npoints = (int)stream.nval;
        
        x = new double[npoints];
        y = new double[npoints];
        z = new double[npoints];
        
        for(int n=0;n<npoints;n++){
            stream.nextToken();
            x[n] = (double)stream.nval;
            stream.nextToken();
            y[n] = (double)stream.nval;
            stream.nextToken();
            z[n] = (double)stream.nval;
        }
    }
    
}
