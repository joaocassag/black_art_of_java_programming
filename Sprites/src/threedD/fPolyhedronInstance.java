
package threedD;

import java.awt.Graphics;

public class fPolyhedronInstance {
    
    protected fArrayOf3dPoints transformedVertices;
    protected fMatrix3d myTransformMatrix;
    protected fPolyhedron thePolyhedron;
    protected fPoint3d myPosition;
    protected fAngle3d myAngle;
    protected boolean positionIsDirty, angleIsDirty;
    
    public fPolyhedronInstance(fPolyhedron poly){
        thePolyhedron = poly;
        try{
            transformedVertices = (fArrayOf3dPoints) thePolyhedron.getVertices().makeClone();
        }catch(Exception e){e.printStackTrace();}
        myPosition = new fPoint3d();
        myAngle = new fAngle3d();
        myTransformMatrix = new fMatrix3d();
    }
    
    public void setOrientation(fPoint3d pos, fAngle3d agl){
        if(myPosition.equals(pos) == false){
            myPosition.set(pos);
            positionIsDirty = true;
        }
        if(myAngle.equals(agl) == false){
            myAngle.set(agl);
            angleIsDirty = true;
        }
    }
    
    public void paint(Graphics g, fGenericCamera camera){
        if(positionIsDirty || angleIsDirty){
            myTransformMatrix.makeMCStoWCStransform(myPosition, myAngle);
            myTransformMatrix.transform(thePolyhedron.getVertices(), transformedVertices);
            positionIsDirty = false;
            thePolyhedron.paint(g, camera.project(transformedVertices));
        }
    }
    
}
