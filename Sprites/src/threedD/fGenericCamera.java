
package threedD;

public class fGenericCamera {
    // -- a temporary buffer used for projection
    protected static fArrayOf2dPoints our2dBuffer = new fArrayOf2dPoints(new int[100],new int[100],100);
    //-- a temporary buffer used for WCS to VCS transform
    protected static fArrayOf3dPoints our3dBuffer = new fArrayOf3dPoints(new double[100],new double[100], new double[100],100);
    // -- the screen distance
    protected double screendist;
    //--screen origo
    protected int x0,y0;
    //--the viewangle
    protected double myViewAngle;
    //-- the matrix used for the WCS to VCS transform
    fMatrix3d myWCStoVCSmatrix;
    //-- mark if the matrix is dirty
    boolean matrixIsDirty;
    //--the position and angle of the camera in WCS
    fPoint3d myPosition;
    fAngle3d myAngle;
    
    public fGenericCamera(int width, int height, double viewAngle){
        myViewAngle = viewAngle;
        x0=width>>1;
        y0=height>>1;
        screendist = (double)x0/(Math.tan(viewAngle/2));
        myWCStoVCSmatrix = new fMatrix3d();
        myPosition = new fPoint3d();
        myAngle = new fAngle3d();
        matrixIsDirty=true;
    }
    
    public void setOrientation(fPoint3d pos, fAngle3d agl){
        if(myPosition.equals(pos) ==false){
            myPosition.set(pos);
            matrixIsDirty=true;
        }
        if(myAngle.equals(agl)==false){
            myAngle.set(agl);
            matrixIsDirty=true;
        }
    }
    
    public fArrayOf2dPoints project(fArrayOf3dPoints p3d){
        updateMatrix();
        myWCStoVCSmatrix.transform(p3d,our3dBuffer);
        for(int n=0;n<p3d.npoints;n++){
            double z = our3dBuffer.z[n];
            our2dBuffer.x[n] =-(int)(screendist*our3dBuffer.x[n]/z)+x0;
            our2dBuffer.y[n] =-(int)(screendist*our3dBuffer.y[n]/z)+y0;
        }
        return our2dBuffer;
    }
    
    private void updateMatrix(){
        if(matrixIsDirty){
            myWCStoVCSmatrix.makeWCStoVCStransform(myPosition,myAngle);
            matrixIsDirty=false;
        }
    }
    
}
