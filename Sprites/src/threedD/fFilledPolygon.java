
package threedD;

import java.awt.Graphics;
import java.io.InputStream;

public class fFilledPolygon extends fIndexingPolygon {
    
    protected fColor myColor;
    
    protected fFilledPolygon(int indices[], int n, fColor color){
        super(indices,n);
        myColor = color;
    }
    
    public fFilledPolygon(InputStream is) throws Exception{
        super(is);
    }

    @Override
    public void paint(Graphics g, int[] x, int[] y) {
        copyIndexedPoints(x,y);
        render(g);
    }
    
    public void render(Graphics g){
        if(orientation()>0){
            g.setColor(myColor.getColor());
            g.fillPolygon(ourScratchyPoly);
        }
    
    }
    
    public String toString(){
        String str = "";
        str = str+" "+myColor.toString()+"\n";
        return str;
    }
    
    public void fromString(InputStream is) throws Exception {
        super.fromString(is);
        myColor = new fColor(is);
    }

    @Override
    public fIndexingPolygon makeClone() {
        int i[];
        System.arraycopy(myIndices,0,i=new int[nbrIndices],0,nbrIndices);
        return new fFilledPolygon(i,nbrIndices,myColor.makeClone());
    }
    
}
