
package threedD;

public class fMatrix3d extends fGeneric3dMatrix {
    
    public fMatrix3d(){
        super();
    }
    
    public void makeMCStoWCStransform(fPoint3d pos, fAngle3d agl, fPoint3d scale){
        makeIdentity();
        concatS(scale.x,scale.y,scale.z);
        concatRx(agl.x);
        concatRy(agl.y);
        concatRz(agl.z);
        concatT(pos.x,pos.y,pos.z);
    }
    
    public void makeMCStoWCStransform(fPoint3d pos, fAngle3d agl){
        makeIdentity();
        concatRx(agl.x);
        concatRy(agl.y);
        concatRz(agl.z);
        concatT(pos.x,pos.y,pos.z);
    }
    
    public void makeWCStoVCStransform(fPoint3d pos, fAngle3d agl){
        makeIdentity();
        concatT(-pos.x,-pos.y,-pos.z);
        concatRx(-agl.x);
        concatRy(-agl.y);
        concatRz(-agl.z);
    }
    /*
    public void makeLookAtPointTransform(fPoint3d po, fPoint3d p1){
        fPoint3d vecZaxis = new fPoint3d(p1,po);
        vecZaxis.normalize(1);
        fPoint3d vecXaxis = new fPoint3d();
        vecXaxis.vectorProduct(new fPoint3d(0,1,0),vecZaxis);
        vecXaxis.normalize(1);
        
        fPoint3d vecYaxis = new fPoint3d();
        vecYaxis.vectorProduct(vecZaxis,vecXaxis);
        
        xx = vecXaxis.x; xy = vecXaxis.y; xz= vecXaxis.z;
        yx = vecYaxis.x; yy = vecYaxis.y; yz= vecYaxis.z;
        zx = vecZaxis.x; zy = vecZaxis.y; zz= vecZaxis.z;
        xo = yo = zo = 0;
        
        xo = xx*(-po.x) + xy*(-po.y) + xz*(-po.z) + xo;
        yo = yx*(-po.x) + yy*(-po.y) + yz*(-po.z) + yo;
        zo = zx*(-po.x) + zy*(-po.y) + zz*(-po.z) + zo;
    }
    */
}
