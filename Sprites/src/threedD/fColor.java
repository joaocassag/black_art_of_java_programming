
package threedD;

import java.awt.Color;
import java.io.InputStream;
import java.io.StreamTokenizer;

public class fColor {
    public int r,g,b;
    protected Color myBaseColor;
    
    public fColor(int r0, int g0, int b0){
        r=r0;
        g=g0;
        b=b0;
        myBaseColor = new Color(r,g,b);
    }
    
    public fColor(InputStream is) throws Exception{
        fromString(is);
    }
    
    public Color getColor(){
        return myBaseColor;
    }
    
    public void fromString(InputStream is) throws Exception{
        StreamTokenizer stream = new StreamTokenizer(is);
        stream.commentChar('#');
        
        stream.nextToken();
        r = (int)stream.nval;
        stream.nextToken();
        g = (int)stream.nval;
        stream.nextToken();
        b = (int)stream.nval;
    }
    
    public String toString(){
        return " "+r+" "+g+" "+b+" ";
    }
    
    public fColor makeClone(){
        return new fColor(r,g,b);
    }
    
}
