
package threedD;

import java.applet.Applet;
import java.awt.Graphics;
import java.io.InputStream;
import static java.lang.Thread.sleep;
import java.net.URL;

public class Cube extends Applet implements Runnable {
    
    fGenericCamera camera;
    fPoint3d CamPos;
    fAngle3d CamAngle;
    
    fPolyhedron cube;
    fPolyhedronInstance cubeInstance[];
    fPoint3d pos[];
    fAngle3d agl;
    
    Thread myThread;
    
    public void init(){
        camera = new fGenericCamera(400,400,Math.PI/2);
        CamPos = new fPoint3d(0,0,5);
        CamAngle = new fAngle3d();
        
        try{
            InputStream is = new URL(getCodeBase(),"cube.f3d").openStream();
            cube = new fConvexPolyhedron(is);
        }catch(Exception e){e.printStackTrace();}
        
        cubeInstance = new fPolyhedronInstance[9];
        for(int n=0;n<9;n++){
            cubeInstance[n] = new fPolyhedronInstance(cube);
        }
        
        pos = new fPoint3d[9];
        int n = 0;
        for(int y=-5;y<=5;y+=5){
            for(int x=-5;x<=5;x+=5){
                pos[n] = new fPoint3d(x,y,0);
                n++;
            }
        }
        agl = new fAngle3d();
        myThread = new Thread(this);
        myThread.start();
    }

    @Override
    public void run() {
        while(true){
            try{sleep(100);}catch(Exception e){}
            agl.x+=Math.PI/20;
            agl.y+=Math.PI/30;
            
            CamPos.z+=0.2;
            CamAngle.z+=Math.PI/50;
            camera.setOrientation(CamPos,CamAngle);
            repaint();
        }
    }
    
    public void start(){
        if(myThread!=null){
            myThread = new Thread(this);
            myThread.start();
        }
    }
    
    public void stop(){
        if(myThread!=null){
            myThread.stop();
            myThread = null;
        }
    }
    
    public void paint(Graphics g){
        g.clearRect(0, 0, getSize().width,getSize().height);
        
        for(int n=0;n<9;n++){
            cubeInstance[n].setOrientation(pos[n], agl);
            cubeInstance[n].paint(g, camera);
        }
    }
    
}
