
package threedD;

import java.awt.Graphics;
import java.io.InputStream;
import java.io.StreamTokenizer;

public abstract class fPolyhedron {
    
    protected fArrayOf3dPoints myVertices;
    protected fIndexingPolygon myPolygons[];
    protected int nbrPolygons;
    
    public fPolyhedron(fArrayOf3dPoints points, fIndexingPolygon polys[], int npolys){
        myVertices = points;
        myPolygons = polys;
        nbrPolygons = npolys;
    }
    
    public fPolyhedron(InputStream is) throws Exception{
        fromString(is);
    }
    
    public abstract void paint(Graphics g, fArrayOf2dPoints point2d);
    
    public String toString(){
        String str = "";
        str = myVertices.toString();
        str=str+nbrPolygons+"\n";
        for(int n=0;n<nbrPolygons;n++){
            str = str+myPolygons[n].toString();
        }
        return str;
    }
    
    public void fromString(InputStream is) throws Exception{
        StreamTokenizer stream = new StreamTokenizer(is);
        stream.commentChar('#');
        
        myVertices  = new fArrayOf3dPoints(is);
        stream.nextToken();
        nbrPolygons=(int) stream.nval;
        myPolygons = new fIndexingPolygon[nbrPolygons];
        for(int n=0;n<nbrPolygons;n++){
            myPolygons[n] = new fFilledPolygon(is);
        }
    }
    
    public fArrayOf3dPoints getVertices(){
        return myVertices;
    }
    
}
