
package threedD;

import java.awt.Graphics;
import java.io.InputStream;

public class fConvexPolyhedron extends fPolyhedron {

    public fConvexPolyhedron(fArrayOf3dPoints points, fIndexingPolygon[] polys, int npolys) {
        super(points, polys, npolys);
    }
    
    public fConvexPolyhedron(InputStream is) throws Exception {
        super(is);
    }

    @Override
    public void paint(Graphics g, fArrayOf2dPoints point2d) {
        for(int n=0;n<nbrPolygons;n++){
            myPolygons[n].paint(g,point2d.x,point2d.y);
        }
    }
    
}
