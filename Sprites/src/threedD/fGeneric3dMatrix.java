
package threedD;

public class fGeneric3dMatrix {
    double xx, xy, xz, xo;
    double yx, yy, yz, yo;
    double zx, zy, zz, zo;
    
    public fGeneric3dMatrix(){
        makeIdentity();
    }
    
    public void makeIdentity(){
        xx=1; xy=0; xz=0; xo=0;
        yx=0; yy=1; yz=0; yo=0;
        zx=0; zy=0; zz=1; zo=0;
    }
    
    public void concatRz(double az){
        double ct = Math.cos(az);
        double st = Math.sin(az);
        
        double Nyx  = (yx * ct + xx *st);
        double Nyy  = (yy * ct + xy *st);
        double Nyz  = (yz * ct + xz *st);
        double Nyo  = (yo * ct + xo *st);
        
        double Nxx  = (xx * ct + yx *st);
        double Nxy  = (xy * ct + yy *st);
        double Nxz  = (xz * ct + yz *st);
        double Nxo  = (xo * ct + yo *st);
        
        xx = Nxx; xy = Nxy; xz = Nxz; xo = Nxo;
        yx = Nyx; yy = Nyy; yz = Nyz; yo = Nyo;
    }
    
    public void concatRy(double ay){
        double ct = Math.cos(ay);
        double st = Math.sin(ay);
        
        double Nxx  = (xx * ct + zx *st);
        double Nxy  = (xy * ct + zy *st);
        double Nxz  = (xz * ct + zz *st);
        double Nxo  = (xo * ct + zo *st);
        
        double Nzx  = (zx * ct + xx *st);
        double Nzy  = (zy * ct + xy *st);
        double Nzz  = (zz * ct + xz *st);
        double Nzo  = (zo * ct + xo *st);
        
        xx = Nxx; xy = Nxy; xz = Nxz; xo = Nxo;
        zx = Nzx; zy = Nzy; zz = Nzz; zo = Nzo;
    }
    
    public void concatRx(double ax){
        double ct = Math.cos(ax);
        double st = Math.sin(ax);
        
        double Nyx  = (xx * ct + zx *st);
        double Nyy  = (xy * ct + zy *st);
        double Nyz  = (xz * ct + zz *st);
        double Nyo  = (xo * ct + zo *st);
        
        double Nzx  = (zx * ct + yx *st);
        double Nzy  = (zy * ct + yy *st);
        double Nzz  = (zz * ct + yz *st);
        double Nzo  = (zo * ct + yo *st);
        
        yx = Nyx; yy = Nyy; yz = Nyz; yo = Nyo;
        zx = Nzx; zy = Nzy; zz = Nzz; zo = Nzo;
    }
    
    public void concatT(double x, double y, double z){
        xo+=x; yo+=y;yo+=y;
    }
    
    public void concatS(double sx, double sy, double sz){
        xx*=sx; xy*=sx; xz*=sx; xo*=sx;
        yx*=sy; yy*=sy; yz*=sy; yo*=sy;
        zx*=sz; zy*=sz; zz*=sz; zo*=sz;
    }
    
    public void transform(fArrayOf3dPoints ps, fArrayOf3dPoints pd){
        for(int i=0;i<ps.npoints;i++){
            double x = ps.x[i], y=ps.y[i], z=ps.z[i];
            pd.x[i] = x * xx + y * xy + z* xz + xo;
            pd.y[i] = x * yx + y * yy + z* yz + yo;
            pd.z[i] = x * zx + y * zy + z* zz + zo;
        }
    }

    
}
