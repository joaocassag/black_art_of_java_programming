
package threedD;

public class fAngle3d {
    public double x, y, z;
    
    public fAngle3d(double x0, double y0, double z0){
        x=x0;
        y=y0;
        z=z0;
    }
    
    public fAngle3d(){
        x=y=z=0;
    }
    
    public fAngle3d(fAngle3d p){
        x=p.x;
        y=p.y;
        z=p.z;
    }
    
    public boolean equals(fAngle3d p){
        return (p.x==x) && (p.y==y) && (p.z==z);
    }
    
    public void set(fAngle3d p){
        x=p.x;
        y=p.y;
        z=p.z;
    }
    
}
