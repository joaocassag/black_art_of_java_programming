
package threedD;

import java.awt.Graphics;
import java.awt.Polygon;
import java.io.InputStream;
import java.io.StreamTokenizer;

public abstract class fIndexingPolygon {

    protected int[] myIndices;
    protected int nbrIndices;
    protected static Polygon ourScratchyPoly = new Polygon(new int[50], new int[50],50);
    
    protected fIndexingPolygon(int indices[], int n){
        myIndices = indices;
        nbrIndices = n;
    }
    
    public fIndexingPolygon(InputStream is) throws Exception{
        fromString(is);
    }
    
    public abstract void paint(Graphics g, int x[], int y[]);
    
    public void fromString(InputStream is) throws Exception{
        StreamTokenizer stream = new StreamTokenizer(is);
        stream.commentChar('#');
        
        stream.nextToken();
        nbrIndices = (int) stream.nval;
        myIndices = new int[nbrIndices];
        for(int i=0; i<nbrIndices;i++){
            stream.nextToken();
            myIndices[i] = (int)stream.nval;
        }
    }
    
    public String toString(){
        String str = "";
        str = str+nbrIndices;
       for(int n=0;n<nbrIndices;n++){
           str = str+" "+myIndices[n];
       }
       return str;
    }
    
    protected void copyIndexedPoints(int x[], int y[]){
        for(int n=0;n<nbrIndices;n++){
            int i = myIndices[n];
            ourScratchyPoly.xpoints[n] = x[i];
            ourScratchyPoly.ypoints[n] = y[i];
        }
        ourScratchyPoly.npoints= nbrIndices;
    }
    
    protected static int orientation(){
        int p1x = ourScratchyPoly.xpoints[1];
        int p1y=ourScratchyPoly.ypoints[1];
        int v1x = ourScratchyPoly.xpoints[2]-p1x;
        int v1y = ourScratchyPoly.ypoints[2]-p1y;
        int v2x = ourScratchyPoly.xpoints[0]-p1x;
        int v2y = ourScratchyPoly.ypoints[0]-p1y;
        
        return v1x*v2y-v2x*v1y;
    }
    
    public abstract fIndexingPolygon makeClone();
}
