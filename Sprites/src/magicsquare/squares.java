
package magicsquare;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextArea;

public class squares extends Applet {
    int squares;
    int length;
    int colors;
    game_board main_board;
    game_board save_board;
    Panel buttons;
    TextArea solve_area;
    Panel main_panel;
    solver solve_thread;
    
    public void init(){
        squares = 3;
        length = 350;
        colors=2;
        main_board = new game_board(squares,length, colors);
        main_board.randomize();
        
        setLayout(new BorderLayout());
        main_panel = new Panel();
        main_panel.setLayout(new BorderLayout());
        
        buttons = new Panel();
        buttons.setLayout(new GridLayout(3,2));
        buttons.add(new Button("More Squares"));
        buttons.add(new Button("Less Squares"));
        buttons.add(new Button("More Colors"));
        buttons.add(new Button("Less Colors"));
        buttons.add(new Button("Solve"));
        buttons.add(new Button("Restore"));
        
        solve_area = new TextArea(8,25);
        solve_area.setEditable(false);
        main_panel.add("North",buttons);
        main_panel.add("South",solve_area);
        
        add("South",main_panel);
    }
    
    public void paint(Graphics g){
        main_board.draw_board(0, 0, g);
    }
    
    public boolean mouseDown(Event ent, int x, int y){
        main_board.apply_move_p(x, y);
        repaint();
        return true;
    }
    
    public boolean action(Event evt, Object arg){
        String label =(String)arg;
        if(label.equals("Solve")){
            if(solve_thread!=null){
                solve_thread.stop();
                solve_thread=null;
            }
            save_board = new game_board(main_board);
            
            solve_thread = new solver(main_board,solve_area);
            solve_thread.setPriority(Thread.MIN_PRIORITY);
            solve_thread.start();
            return true;
        }
        if(label.equals("Restore")){
            if(save_board==null)
                return true;
            
            main_board = new game_board(save_board);
            
            repaint();
            return true;
        }
        
        if(label.equals("More Colors")){
            colors++;
        } else if(label.equals("Less Colors")){
            colors--;
        } else if(label.equals("Less Squares")){
            squares-=2;
        } else if(label.equals("More Squares")){
            squares+=2;
        } 
        
        if(squares>35)
            squares = 35;
        if(squares<3)
            squares=3;
        if(colors>5)
            colors = 5;
        if(colors<2)
            colors=2;
        
        main_board = new game_board(squares,length,colors);
        main_board.randomize();
        
        repaint();
        return true;
    }
}
