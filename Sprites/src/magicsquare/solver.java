
package magicsquare;

import java.awt.TextArea;

public class solver extends Thread {
    
    game_board start_board;
    TextArea solve_window;
    int max_depth=12;
    int current_depth=1;
    int visited =0;
    int solved[][] = new int[max_depth+1][2];
    
    public solver(game_board board, TextArea t){
        start_board = new game_board(board);
        solve_window = t;
    }
    
    public void run(){
        int i;
        visited = 0;
        solve_window.append("Beginning solve.\n");
        for(i=1;i<max_depth;i++){
            solve_window.append("Attempting depth search of "+i+"\n ");
            if(try_all_squares(i) == true){
                break;
            }
            solve_window.append("No solution found in "+visited+" boards.\n");
        }
        solve_window.append("Done solving\n");
    }
    
    public boolean try_all_squares(int depth){
        int i, j, k, l,m;
        i=j=0;
        if(start_board.is_completed()){
            print_solution();
            return true;
        }
        
        if(current_depth>depth)
            return false;
        try{sleep(7);}catch(Exception e){}
        for(i=0;i<start_board.side_squares;i++)
            for(j=0;j<start_board.side_squares;j++){
                start_board.apply_move_s(i, j);
                solved[current_depth][0]=i;
                solved[current_depth][1]=j;
                
                if(is_repeat()){
                    start_board.undo_move(i,j);
                    continue;
                }
                visited++;
                
                if(start_board.is_completed()){
                    print_solution();
                    return true;
                }
                else{
                    current_depth++;
                    if(try_all_squares(depth)==true)
                        return true;
                    else
                        current_depth--;
                }
                start_board.undo_move(i,j);
            }
        start_board.undo_move(i,j);
        return false;
    }

    public void print_solution() {
        int m;
        solve_window.append("Board solution found after "+visited+" boards.");
        solve_window.append("\nMoves are:\n");
        if(current_depth>=1 && visited>0)
            for(m=1;m<=current_depth;m++)
                solve_window.append(solved[m][0]+1+","+(solved[m][1]+1)+"\n");
        else
            solve_window.append("Board already solved.\n");
    }
    
    public boolean is_repeat(){
        int tx, ty,j;
        int count=0;
        
        if(current_depth<start_board.max_color+1)
            return false;
        
        tx = solved[current_depth][0];
        ty = solved[current_depth][1];
        
        j = current_depth;
        
        while((tx==solved[j][0]) && (ty==solved[j][1]) && j>=1){
            count++;
            j--;
        }
        return(count>=start_board.max_color+1);
    }
    
}
