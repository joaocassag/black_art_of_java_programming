
package magicsquare;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class game_board {
    int board[][];
    int side_squares;
    int max_color;
    int side_length;
    
    public game_board(int squares, int side_pixels, int colors){
        board = new int[squares][squares];
        side_squares = squares;
        side_length = side_pixels;
        max_color = colors -1;
    }
    
    public game_board(game_board source){
        board = new int[source.side_squares][source.side_squares];
        side_squares = source.side_squares;
        max_color = source.max_color;
        side_length = source.side_length;
        copy_board(source);
    }
    
    public void randomize(){
        Random num_gen = new Random();
        int i,j;
        for(i=0;i<side_squares;i++)
            for(j=0;j<side_squares;j++){
                board[i][j]=(num_gen.nextInt()%(max_color+1));
                if(board[i][j]<0)
                    board[i][j]*=-1;
            }
    }
    
    public void increment_square(int x1, int y1){
        if(x1<0||x1>side_squares-1||y1<0||y1>side_squares-1)
            return;
        else{
            board[x1][y1]++;
            if(board[x1][y1]>max_color)
                board[x1][y1]=0;
        }
    }
    
    public void apply_move_s(int x, int y){
        increment_square(x,y);
        increment_square(x-1,y);
        increment_square(x+1,y);
        increment_square(x,y-1);
        increment_square(x,y+1);
    }
    
    public void apply_move_p(int xpix, int ypix){
        int x,y;
        if(xpix>side_length||ypix>side_length)
            return;
        x = xpix/((int)(side_length/side_squares));
        y = ypix/((int)(side_length/side_squares));
        
        apply_move_s(x,y);
    }
    
    public void draw_board(int x, int y, Graphics g){
        int i, j;
        for(i=0;i<side_squares;i++)
            for(j=0;j<side_squares;j++)
                fill_square(x+i,y+j,g);
    }
    
    public void fill_square(int x1, int y1, Graphics g){
        int x, y, side;
        
        if(board[x1][y1]==0)
            g.setColor(Color.lightGray);
        else if(board[x1][y1]==1)
            g.setColor(Color.red);
        else if(board[x1][y1]==2)
            g.setColor(Color.blue);
        else if(board[x1][y1]==3)
            g.setColor(Color.green);
        else if(board[x1][y1]==4)
            g.setColor(Color.pink);
        else if(board[x1][y1]==5)
            g.setColor(Color.yellow);
        
        x = x1*(int)(side_length/side_squares)+1;
        y = y1*(int)(side_length/side_squares)+1;
        
        side = (int)(side_length/side_squares)-1;
        g.fillRect(x, y, side, side);
    }
    
    public boolean copy_board(game_board source){
        if(source.side_squares!=side_squares)
            return false;
        int i,j;
        for(i=0;i<side_squares;i++)
            for(j=0;j<side_squares;j++)
                board[i][j] = source.board[i][j];
        return true;
    }
    
    public boolean is_completed(){
        int color;
        int i,j;
        color = board[0][0];
        for(i=0;i<side_squares;i++)
            for(j=0;j<side_squares;j++)
            if(board[i][j]!= color)
                return false;
        return true;
    }

    public void undo_move(int x, int y) {
        int i;
        for(i=0;i<max_color;i++)
            apply_move_s(x,y);
    }
}
