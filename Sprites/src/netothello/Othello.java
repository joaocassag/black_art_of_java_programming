
package netothello;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Panel;
import java.awt.TextArea;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.TextField;
import java.util.StringTokenizer;

public class Othello extends Applet implements Runnable {
    
    final boolean WHITE = true;
    final boolean BLACK = false;
    
    //the game stuff
    GameBoard theBoard;
    boolean turn;
    boolean local;
    
    //the GUI stuff
    TextArea dispA;
    Panel inputPanel, turnPanel;
    promptFrame pf;
    
    Thread kicker;
    
    //the newtwork stuff
    PrintStream ps;
    Socket s = null;
    DataInputStream dis = null;
    String name, theHost="localhost";
    int port = 8314;
    
    Image im[];
    
    public void init(){
        im = new Image[3];
        im[0] = getImage(getCodeBase(), "images/empty.gif");
        im[1] = getImage(getCodeBase(), "images/white.gif");
        im[2] = getImage(getCodeBase(), "images/black.gif");
        
        // set up all GUI components
        setLayout(new BorderLayout());
        inputPanel = new Panel();
        inputPanel.setLayout(new BorderLayout());
        inputPanel.add("West",dispA=new TextArea(5,35));
        inputPanel.add("South",new TextField(20));
        inputPanel.add("Center", turnPanel = new Panel());
        turnPanel.add(new Button("Logout"));
        turnPanel.add(new Button("New Game"));
        newGame();
        
        //check applet parameters
        try{
            port = Integer.valueOf(getParameter("port")).intValue();
        }catch(Exception e){port = 8314;}
    }
    
    public void newGame(){
        kicker = new Thread(this);
        kicker.setPriority(Thread.MIN_PRIORITY);
        kicker.start();
        
        initBoard();
        promptUser();
    }
    
    public void initBoard(){
        turn = WHITE;
        theBoard = new GameBoard(8,8, im);
        theBoard.addPiece(3,3,WHITE);
        theBoard.addPiece(3,4,BLACK);
        theBoard.addPiece(4,3,BLACK);
        theBoard.addPiece(4,4,WHITE);
    }
    
    public void paint(Graphics g){
        Dimension d = getSize();
        d.height-= inputPanel.getPreferredSize().height;
        theBoard.paintBoard(g, new Rectangle(0,0,d.width, d.height));
    }
    
    /*v1
    public boolean mouseDown(Event evt, int x, int y){
        int xx, yy;
        xx = (int)(x/theBoard.pieceWidth);
        yy = (int)(y/theBoard.pieceHeight);
        theBoard.addPiece(xx,yy,turn!=turn);
        repaint(xx*theBoard.pieceWidth,yy*theBoard.pieceHeight, theBoard.pieceWidth, theBoard.pieceHeight);
        return true;
    }*/
    
    /*v2
    public boolean mouseDown(Event evt, int x, int y){
        if(validMoveXY(x,y,turn)){
            int xx, yy;
            xx = (int)(x/theBoard.pieceWidth);
            yy = (int)(x/theBoard.pieceWidth);
            doMove(xx,yy,turn);
            turn = !turn;
            return true;
        }
        return false;
    }
    */
    public boolean mouseDown(Event evt, int x, int y){
        if(turn == local){
            if(validMoveXY(x,y,turn)){
                int xx, yy;
                xx = (int)(x/theBoard.pieceWidth);
                yy = (int)(y/theBoard.pieceHeight);
                boolean flag = false;
                //be sure the move gets sent
                while(!false)
                    try{
                        ps.println("move|"+xx+"|"+yy);
                        ps.flush();
                        System.out.println("Sent move: "+xx+","+yy);
                        flag=true;
                        return true;
                    }catch(Exception e){flag = false;}
                //return true;
            }
        }
        return false;
    }
    
    public boolean validMoveXY(int x, int y, boolean color){
        return validMove((int) (x/theBoard.pieceWidth), (int)(y/theBoard.pieceHeight), color);
    }
    
    public boolean validMove(int x, int y, boolean color){
        if(theBoard.pieceAt(x, y)!=null) return false;
        
        int a, b, i, j, num;
        GamePiece temp = null;
        
        for(i=-1;i<=1;i++)
            for(j=-1;j<=1;j++)
                if(!(j==0 && i==0)){
                    a = x;
                    b = y;
                    num = 0;
                    
                    do{
                        a+=i;
                        b+=j;
                        temp = theBoard.pieceAt(a, b);
                        if(temp!=null && temp.color == !color)
                            num++;
                        if(temp!=null && temp.color == color)
                            if(num>0) return true;
                            else temp = null;
                    }while(temp!=null);
                }
        return false;
    }
    
    public int validMoves(boolean color){
        int i, j, num=0;
        for(i=0;i<8;i++)
            for(j=0;j<8;j++)
                if(validMove(i,j,color))
                    num++;
        return num;
    }
    
    public void doMove(int x, int y, boolean color){
        int a =x, b=y, i, j, num;
        GamePiece temp = null;
        
        theBoard.addPiece(x,y,color);
        repaint(a*theBoard.pieceWidth, b*theBoard.pieceHeight, theBoard.pieceWidth, theBoard.pieceHeight);
        
        for(i=-1;i<=1;i++)
            for(j=-1;j<=1;j++)
                if(!(j==0 && i==0)){
                    a=x;
                    b=i;
                    num=0;
                    do{
                        a+=i;
                        b+=j;
                        temp = theBoard.pieceAt(a, b);
                        if(temp!=null){
                            if(temp.color == !color)
                                num++;
                            else if(temp.color == color){
                                if(num>0){
                                    a=x+i;
                                    b=y+j;
                                    temp = theBoard.pieceAt(a, b);
                                    while(temp.color ==!color){
                                        theBoard.pieceAt(a,b).flip();
                                        repaint(a*theBoard.pieceWidth, b*theBoard.pieceHeight, theBoard.pieceWidth,theBoard.pieceHeight);
                                        a+=i;
                                        b+=j;
                                        temp = theBoard.pieceAt(a, b);
                                    }
                                }
                                temp = null;
                            }
                        }
                    }while(temp!=null);
                }
        int bl, wh;
        if(validMoves(!turn)>0){
            turn = !turn;
        }
        
        if(endGame())
            if( (wh=theBoard.count(WHITE)) > (bl=theBoard.count(BLACK)))
                display("White wins!");
            else if(wh<bl)
                display("Black wins!");
            else if (bl == wh)
                display("It's a TIE!");
    }
    
    public boolean endGame(){
        if(theBoard.empty==0)
            return true;
        else if(theBoard.count(BLACK) == 0 || theBoard.count(WHITE) ==0)
            return true;
        else if(validMoves(BLACK) == 0 && validMoves(WHITE) == 0)
            return true;
        return false;
    }
    
    public void display(String str){
        System.out.println("str");
    }
    
    public boolean handleEvent(Event evt){
        if(evt.target instanceof TextField && evt.id == Event.ACTION_EVENT){
            ps.println("say|"+name+" says: "+((TextField)evt.target).getText());
            ((TextField)evt.target).setText("");
            return true;
        }
        if(evt.target instanceof Button && evt.arg.equals("Restart")){
            kicker.stop();
            newGame();
            repaint();
            return true;
        }
        return super.handleEvent(evt);
    }
    
    public void promptUser(){
        pf = new promptFrame();
        pf.setSize(300,100);
        pf.setVisible(true);
    }

    @Override
    public void run() {
        //first wait for pf to get a name from the user
        name = null;
        while(name ==null){
            name = pf.gotName;
            try{kicker.sleep(500);}catch(Exception e){}
        }
        pf.dispose();
        
        s = null;
        //make socket connections
        while(s==null)
            try{
                theHost = getCodeBase().getHost();
                display("Attempting to make connection");
                s= new Socket(theHost, port);
                dis = new DataInputStream(s.getInputStream());
                ps = new PrintStream(s.getOutputStream());
            }catch(Exception e){
                try{kicker.sleep(7500);}catch(Exception e1){}
            }
        display("Connection established");
        display("Waiting for another player ...");
        
        //main loop event
        while(kicker !=null){
            String input = null;
            StringTokenizer st = null;
            
            while(input == null)
                try{
                    kicker.sleep(100);
                    input = dis.readLine();
                }catch(Exception e){
                    input = null;
                }
            System.out.println("Got: "+input);
            
            //if the other player disconnected for any reason ... start over
            if(input.equals("bye")){
                display("Your partner has left the game ... Restarting");
                newGame();
                repaint();
                return;
            }
            
            st = new StringTokenizer(input,"|");
            String cmd = st.nextToken();
            String val = st.nextToken();
            //if we are ready to start a game
            if(cmd.equals("start")){
                display("Got another player.");
                if(val.equals("blakc")) local = BLACK;
                else local = WHITE;
                display("You will play "+val);
                repaint();
            } 
            else if(cmd.equals("move"))
                doMove(Integer.parseInt(val), Integer.parseInt(st.nextToken()),turn);
            else if(cmd.equals("say"))
                display(val);
        }
        
    }
    
    public void stop(){
        try{
            ps.println("bye");
            dis.close();
            ps.close();
            s.close();
        }catch(Exception e){}
    }
}
