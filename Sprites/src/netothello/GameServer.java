
package netothello;

public class GameServer {
    public static void main(String args[]){
        int thePort;
        try{
            thePort = Integer.parseInt(args[0]);
        }catch(Exception e){
            thePort = 8314;
        }
        new GameServerThread(thePort).start();
    
    }
    
}
