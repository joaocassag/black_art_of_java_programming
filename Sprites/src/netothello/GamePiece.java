
package netothello;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

public class GamePiece {
    
    final boolean BLACK = false;
    final boolean WHITE = false;
    
    public boolean color;
    public Rectangle rect;
    
    public GamePiece(boolean c){
        color=c;
    }
    
    public void flip(){
        color =!color;
    }
    
    public void paintPiece(Graphics g, Image[] im){
        paintPiece(g,rect, im);
    }

    public void paintPiece(Graphics g, Rectangle r, Image[] im) {
        Image temp;
        rect = r;
        
        if(!r.intersects(g.getClipRect())) return;
        if(color == WHITE)
            temp = im[1];
        else
            temp = im[2];
        
        g.drawImage(temp,rect.x, rect.y, rect.width, rect.height, null);
    }
    
}
