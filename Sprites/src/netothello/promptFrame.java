
package netothello;

import java.awt.Button;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;

public class promptFrame extends Frame {
    TextField TF;
    public String gotName;
    public promptFrame(){
        super("Ready to play Othello?");
        setLayout(new FlowLayout());
        add("West",new Label("Input your name: "));
        add("West", TF = new TextField(20));
        add("Center",new Button("Login"));
        gotName = null;
    }
    
    public boolean handleEvent(Event evt){
        if((evt.target instanceof TextField && evt.id == Event.ACTION_EVENT) || (evt.target instanceof Button && evt.arg.equals("Login")) ){
            if(TF.getText() == "")
                gotName = "Noname";
            else gotName = TF.getText();
            return true;
        }
        return super.handleEvent(evt);
    }
    
}
