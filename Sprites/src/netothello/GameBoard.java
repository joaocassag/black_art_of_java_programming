
package netothello;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

public class GameBoard {
    
    final boolean BLACK = false;
    final boolean WHITE = false;
    
    public int pieceWidth, pieceHeight;
    public int rows, cols;
    GamePiece board[][];
    public int empty;
    Image im[];
    
    public GameBoard(int w, int h, Image images[]){
        board = new GamePiece[w][h];
        cols = w;
        rows = h;
        empty=w*h;
        im = images;
    }
    
    public void paintBoard(Graphics g, Rectangle r){
        
        pieceWidth = (int)(r.width/cols);
        pieceHeight = (int)(r.height/rows);
        
        g.setColor(Color.green);
        g.fillRect(r.x,r.y,r.width,r.height);
        
        for(int x=0;x<cols;x++){
            for(int y=0;y<rows;y++){
                if(board[x][y]!=null)
                    board[x][y].paintPiece(g, 
                            new Rectangle(r.x+pieceWidth*x,r.y+pieceHeight*y,pieceWidth,pieceHeight),im);
                else
                    g.drawImage(im[0],r.x+pieceWidth*x,r.y+pieceHeight*y,pieceWidth,pieceHeight,null);
            }
        }
    }
    
    public GamePiece pieceAt(int x, int y){
        if(x>=cols || x<0 || y>=rows || y<0) return null;
        return board[x][y];
    }
    
    public GamePiece pieceAtXY(int x, int y){
        for(int i=0;i<cols;i++)
            for(int j=0;j<rows;j++)
                if(board[i][j].rect.inside(x, y))
                    return board[i][j]; 
        return null;
    }
    
    public int count(boolean color){
        int num = 0;
        for(int i=0;i<cols;i++)
            for(int j=0;j<rows;j++)
                if(board[i][j]!=null && board[i][j].color == color)
                    num++;
        return num;
    }

    public void addPiece(int x, int y, boolean color) {
        board[x][y] = new GamePiece(color);
    }
    
}
