
package advancedTechniques;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class CycleColorActor extends Actor {
    
    protected int locx, locy;
    protected int red, green, blue;
    protected int delay;
    
    public CycleColorActor(Applet a, int x, int y, int r, int g, int b, int d){
        applet = a;
        drawNow = false;
        
        locx = x;
        locy = y;
        red = r;
        green = g;
        blue = b;
        delay = d;
        start();
    }
    
    public void run(){
        while(true){
            drawNow = true;
            applet.repaint();
            updateColor();
            try{sleep(delay);}catch(Exception e){}
        }
    }
    
    public void updateColor(){
        red = (red+4)%256;
        blue = (blue+4)%256;
        green = (green+4)%256;
    }

    @Override
    public void paint(Graphics g) {
        if(drawNow){
            g.setColor(new Color(red,green,blue));
            g.fillRect(locx, locy, 73, 73);
            drawNow = false;
        }
    }
    
}
