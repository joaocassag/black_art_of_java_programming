
package advancedTechniques;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;

public class ExtractImageTest extends Applet {
    Image strip;
    Image images[];
    int num_images=0;
    int width = 20;
    int height;
    
    public void init(){
        Image strip = getImage(getCodeBase(),"alien.gif");
        
        while(strip.getWidth(this)<0){}
        
        num_images = strip.getWidth(this)/width;
        
        height = strip.getHeight(this);
        
        images = new Image[num_images];
        
        extractImages(strip,images,num_images,width,height);
    }
    
    public void extractImages(Image strip, Image images[], int num_images, int width, int height){
        ImageProducer source = strip.getSource();
        
        for(int i =0; i< num_images;i++){
            ImageFilter extractFilter = new CropImageFilter(i*width, 0, width, height);
            
            ImageProducer producer = new FilteredImageSource(source, extractFilter);
            images[i] = createImage(producer);
        }
    }
    
    public void paint(Graphics g){
        for(int i = 0;i<num_images;i++){
            g.drawImage(images[i], i*width, i*13, this);
        }
    }
    
}
