
package advancedTechniques;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class ActorTest extends Applet {
    
    Actor actors[] = new Actor[4];
    
    public void init(){
        int width = getSize().width;
        int height = getSize().height;
        setBackground(Color.black);
        
        actors[0] = new CycleColorActor(this,0,0,90,150,0,50);
        actors[1] = new CycleColorActor(this,width-73,0,90,150,0,100);
        actors[2] = new CycleColorActor(this,0,height-73,90,150,0,200);
        actors[3] = new CycleColorActor(this,width-73,height-73,90,150,0,400);
    }
    
    public void update(Graphics g){
        paint(g);
    }
    
    public void paint(Graphics g){
        for(int i =0; i<actors.length;i++){
            actors[i].paint(g);
        }
    }
    
}
