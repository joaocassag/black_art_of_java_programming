
package advancedTechniques;

import java.applet.Applet;
import java.awt.Graphics;

public abstract class Actor extends Thread {
    Applet applet;
    protected boolean drawNow;
    abstract void paint(Graphics g);
    
}
