
package NetworkedHighScoreClientServer;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.StringTokenizer;

public class HighScoreManager implements Runnable {
    
    int NUM_SCORES;
    HighScoreList L;
    
    private int red, green, num;
    private int fontSize;
    private int where;
    
    private int last = 0;
    
    String updateStr="";
    
    Thread kicker = null;
    
    final int DELAY = 10_000;
    
    /*HighScoreManager(){
        newColors();
    }*/
    
    public HighScoreManager(int max){
        
        NUM_SCORES = max;
        L = new HighScoreList(NUM_SCORES);
        kicker = new Thread(this);
        kicker.start();
        newColors();
        
    }
    
    public String getScoreData(){
        //return "Eric & 100 | Dave & 200 | Jane & 0 | <ary & 24235";
        Socket s;
        DataInputStream ds;
        PrintStream ps;
        String temp = null;
        
        try{
            s = new Socket("localhost",2357);
            ds = new DataInputStream(s.getInputStream());
            ps = new PrintStream(s.getOutputStream());
        }catch(Exception e){return null;}
        
        StringTokenizer st;
        try{
            if(updateStr!=null){
                st = new StringTokenizer(updateStr,"|");
                while(st.hasMoreTokens())
                    ps.println("udpate::"+st.nextToken());
                updateStr="";
                
                ps.println("request::"+last);
                temp = ds.readLine().trim();
                ps.println("bye");
            }
        }catch(Exception e){return null;}
        
        try{
            ds.close();
            ps.close();
            s.close();
        }catch(Exception e){}
        
        return temp;
    }
    
    public void getScores(){
        String str;
        int x = 0;
        str = getScoreData();
        if(str == null || str == "none") return;
        L = new HighScoreList(NUM_SCORES, str);
    }
    
    public void paintScores(Graphics g, Rectangle r){
        int x,i;
        int b = 255;
        
        for(x=r.x; x < r.width + r.x; x+= (int)(r.width/num)){
            b-=(int)(255/num);
            if(b<0) b=0;
            g.setColor(new Color(red, green, b));
            
            //Rectangle rn = g.getClipBounds();
            //if(rn == null){System.out.println("rn is null");g.setClip(0, 0, 400, 400);}
            if(g.getClipBounds().intersects(new Rectangle(x,r.y,r.width,r.height))){
                //g.setClip(null);
                g.fillRect(x, r.y, r.width, r.height);
            }
        }
        
        fontSize = (int) (r.height/(NUM_SCORES+4));
        g.setFont(new Font("Times New Roman", Font.PLAIN, fontSize));
        FontMetrics fm = g.getFontMetrics();
        where = 5+fontSize;
        g.setColor(Color.black);
        String str = "High Scores";
        g.drawString(str, (int)((r.width+r.x-fm.stringWidth(str))/2),where);
        
        for(x = 0; x< NUM_SCORES; x++){
            where +=fontSize + 5;
            if(g.getClipRect().intersects(new Rectangle(r.x, where - fontSize-5, r.width, fontSize))){
                str = "  "+(x+1);
                if(L.getScore(x)!=null){
                    str+=L.getScore(x).name;
                    for(i = 5+fm.stringWidth(str);i<r.x+r.width-6-fm.stringWidth(" "+L.getScore(num).score);i+=fm.stringWidth("."))
                        str +=".";
                    str +=" "+L.getScore(x).score+"   ";
                }
                g.setColor(Color.black);
                g.drawString(str, r.x,where);
            }
        }
    }
    
    public void newColors(){
        red = (int) (Math.random()*200)+56;
        green = (int) (Math.random()*200)+56;
        num = (int) (Math.random()*50)+2;
    }
    
    public void addScore(String name, float score, String other){
        String temp;
        temp = L.tryScore(name, score, other);
        if(temp !=null) updateStr+=temp+"|";
    
    }
    
    public HSob getHighestScore(){
        return L.getScore(last=0);//TODO: lastChange??
    }
    
    public HSob getNextScore(){
        if(last++ >= NUM_SCORES) return null;
        return L.getScore(last);
    }
    
    public HSob getScore(int x){
        if(x > NUM_SCORES) return null;
        return L.scores[last=(x-1)];
    }

    @Override
    public void run() {
        while(kicker !=null){
            getScores();
            try{kicker.sleep(DELAY);}
            catch(Exception e){System.out.println("An error occurred due to "+e.getMessage());}
        }
    }
    
    public void start(){
        if(kicker == null){
            kicker = new Thread(this);
        }
    }
    
    public void stop(){
        kicker = null;
    }
}
