
package NetworkedHighScoreClientServer;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.TextField;

public class testApp extends Applet {
    
    HighScoreManager HS;
    Image im;
    Graphics g;
    
    Panel p = new Panel();
    TextField F;
    
    public void init(){
        HS = new HighScoreManager(10);
        
        setLayout(new BorderLayout());
        add("South",p);
        
        p.add(new Button("Change Color"));
        p.add(new Button("Add"));
        
        F = new TextField(10);
        p.add(F);
    }
    
    public void paint(Graphics bg){
        System.out.println("paint");
        Rectangle r;
        int panel_height = p.preferredSize().height;
        r = new Rectangle(0,0,size().width,size().height-panel_height);
        //r = new Rectangle(0,0,size().width, size().height);
        HS.paintScores(bg, r);
        if(im!=null)
            bg.drawImage(im, 0, 0, null);
    }
    
    public void update(Graphics bg){
        System.out.println("update");
        //Rectangle r = new Rectangle(0,0,size().width,size().height);
        
        int panel_height = p.getPreferredSize().height;
        Rectangle r = new Rectangle(0,0,getSize().width,getSize().height-panel_height);
        
        im = createImage(getSize().width,getSize().height);
        g = im.getGraphics();
        HS.paintScores(g, r);
        paint(bg);
    }
    
    public boolean action(Event evt, Object arg){
        if("Change Color".equals(arg)){
            HS.newColors();
            repaint();
            return true;
        }
        if("Add".equals(arg)){
            HS.addScore(F.getText(), (int)(Math.random()*1000),"TestAdd");
            F.setText(null);
            repaint();
            return true;
        }
        return false;
    }
    
}
