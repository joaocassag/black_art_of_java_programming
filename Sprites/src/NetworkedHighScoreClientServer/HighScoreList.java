
package NetworkedHighScoreClientServer;

import java.util.StringTokenizer;

public class HighScoreList {
    int NUM_SCORES=10;
    public long lastChange=0;
    
    HSob scores[];
    
    HighScoreList(int num){
        NUM_SCORES = num;
        scores = new HSob[NUM_SCORES];
    }

    HighScoreList(int num, String str) {
        this(num);
    }
    
    public void parseData(String str){
        StringTokenizer st1 = new StringTokenizer(str,"|");
        String theStr;
        while(st1.hasMoreTokens()){
            theStr = st1.nextToken();
            if(!theStr.startsWith("null"))
                addScore(new HSob(theStr));
        }
    }

    public int addScore(HSob sc) {
        int x, i;
        x=0;
        if(sc ==null) return 0;
        while(x < NUM_SCORES){
            if(scores[x] == null || sc.score > scores[x].score){
                for(i = NUM_SCORES-2;i >= x; i--){
                    scores[i+1] = scores[i];
                    scores[x] = sc;
                    lastChange = System.currentTimeMillis();
                    return x+1;
                }
                x++;
            }
        }
        return 0;
    }
    
    public String tryScore(String name, float score, String other){
        HSob temp;
        temp = new HSob(name, score,other);
        return tryScore(temp.toDataString());
    }

    public String tryScore(String data) {
        HSob temp = new HSob(data);
        if(addScore(temp) > 0)
            return temp.toDataString();
        else return null;
        
    }
    
    public HSob getScore(int num){
        if(num < NUM_SCORES)
            return scores[num];
        else return null;
    
    }
}

class HSob{
        public String name, other;
        public float score;
        
        HSob(String n, float sc){
            name = n;
            score = sc;
        }
        
        HSob(String n, float sc, String ot){
            name = n;
            score = sc;
            other = ot;
        }
        
        HSob(){
            name = "noname";
            score=0;
        }
        
        HSob(String data){
            Float fl = null;
            String str;
            StringTokenizer st = new StringTokenizer(data,"&");
            while(st.hasMoreTokens()){
                str = st.nextToken();
                if(name == null)
                    name = str;
                else{
                    try{
                        fl = Float.valueOf(str);
                        score = fl.floatValue();
                    }catch(Exception e){
                        other = str;
                    }
                }
                
            }
        }
        
        public String toDataString(){
            return name + "&" + score + "&" + other;
        }
    }
