
package NetworkedHighScoreClientServer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class SaveFileThread extends Thread {
    
    HighScoreList L;
    final int FILE_DELAY=100_000;
    File f;
    long lastWrite;
    
    public SaveFileThread(HighScoreList theList, File theFile){
        L = theList;
        f = theFile;
    }
    
    public void run(){
        while (L!=null){
            if(f.exists())
                f.renameTo(new File("test.bak"));
            
            if(L.lastChange > lastWrite){
                try{
                    PrintStream ps = new PrintStream(new FileOutputStream(f));
                    ps.println(L.toString());//TODO call toDataString() ie. create one
                    System.out.println("Writing High Scores to file "+f.getName());
                    lastWrite = System.currentTimeMillis();
                    ps.close();
                }catch(Exception e){}
            }
            try{sleep(FILE_DELAY);}catch(Exception e){}
        }
    }
    
}
