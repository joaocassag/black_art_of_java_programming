
package NetworkedHighScoreClientServer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

public class ServerThread  extends Thread{
    ServerSocket servSock = null;
    long lastChange;
    HighScoreList L;
    
    public ServerThread(HighScoreList theList){
        try{
            servSock = new ServerSocket(2357);
        }
        catch(Exception e){
            System.out.println("Failed to initialize the server due to "+e.getMessage());
            System.exit(1);
        }
        L = theList;
    }
    
    public void run(){
        lastChange = System.currentTimeMillis();
        while(servSock!=null && L!=null){
            Socket sock =null;
            try{
                sock = servSock.accept();
                System.out.println("Got connection: "+2357);
            }
            catch(Exception e){ 
                System.out.println("Connection request failed");
                System.exit(1);
            }
            
            try{
                DataInputStream dis = new DataInputStream(
                                        new BufferedInputStream(sock.getInputStream()));
                PrintStream ps = new PrintStream(
                                    new BufferedOutputStream(sock.getOutputStream(),1024),
                                    false);
                String inputLine = null, outputLine = "\n";
                String command = "blah";
                while(command!=null && !command.startsWith("bye")){
                    StringTokenizer st1 = null;
                    inputLine = dis.readLine().trim();
                    
                    if(inputLine!=null){
                        System.out.println("Got: "+inputLine);
                        st1 = new StringTokenizer(inputLine,"::");
                    }
                    
                    if(st1!=null && st1.hasMoreTokens()){
                        command = st1.nextToken();
                        if(command.startsWith("request")){
                            if(st1.hasMoreTokens() && Long.valueOf(st1.nextToken()).longValue() < lastChange)
                                outputLine = L.toString();//TODO call toDataString() ie. create one
                            else outputLine = "none";
                            ps.println(outputLine);
                            ps.flush();
                        }
                        if(command.startsWith("update")){
                            if(L.tryScore(st1.nextToken()) !=null)
                                lastChange = System.currentTimeMillis();
                        }
                    }
                }
                ps.close();
                dis.close();
                sock.close();
            }catch(Exception e){}
        }
    
    }
    
    public void finalize(){
        try{servSock.close();}
        catch(Exception e){}
        servSock = null;
    }
    
}
