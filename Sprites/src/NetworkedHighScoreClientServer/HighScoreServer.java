
package NetworkedHighScoreClientServer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class HighScoreServer {
    
    static ServerSocket servSock;
    
    public static void main(String[] args){
        System.out.println("Hello from the server application.");
        
        int values=10;
        HighScoreList theList = null;
        File theFile = new File(args[1]);
        String temp = null;
        
        if(!theFile.exists()){
            System.out.println("No such file\nWill create a new one");
        } else{
            try{
                DataInputStream dis = new DataInputStream( new FileInputStream(theFile));
                temp = dis.readLine();
            }catch(Exception e){
                System.out.println("Unable to read from file. Terminating...");
                System.exit(1);
            }
        }
        
        if(temp != null)
            theList = new HighScoreList(Integer.valueOf(args[0]).intValue(),temp);
        else
            theList = new HighScoreList(Integer.valueOf(args[0]).intValue());
        
        if(theList !=null){
            System.out.println("Unable to initialize. Terminating ...");
            System.exit(1);
        }
        
        new ServerThread(theList).start();
        new SaveFileThread(theList,theFile).start();
        System.out.println("Server initialized. Tracking "+args[0]+" scores.\nUsing "+args[1]);
        
        //HighScoreList theList = new HighScoreList(Integer.valueOf(args[0]).intValue(),
        //HighScoreList theList = new HighScoreList(Integer.valueOf(values).intValue(),
        //                                   "Bob&100|Eric&2000");
        //System.out.println("Server tracking "+args[0]+" scores");
        //System.out.println("Server tracking "+values+" scores");
        /*
        try{
            servSock = new ServerSocket(2357);
        }catch(Exception e){
            System.out.println("Tried to setup serverSocket, but failed");
            System.exit(1);
        }
        
        Socket sock = null;
        DataInputStream dis = null;
        PrintStream ps = null;
        try{
            sock = servSock.accept();
            System.out.println("Got connetion: "+2357);
            dis = new DataInputStream(
                    new BufferedInputStream(sock.getInputStream()));
           ps = new PrintStream(
                   new BufferedOutputStream(sock.getOutputStream(),1024),false);
           
        }catch(Exception e){
            System.out.println("Could  not het connection");
            System.exit(1);
        }
        
        try{
            String inputLine="";
            while(!inputLine.startsWith("bye")){
                inputLine = dis.readLine();
                System.out.println("Got: "+inputLine);
                if(inputLine.startsWith("request"))
                    ps.println("none");
            }
        }catch(Exception e){}
        
        try{
            dis.close();
            ps.close();
            sock.close();
        }catch(Exception e){System.exit(1);}
    */
    }
}
