
package gui;

import AlienLandingGame.GameManager;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.Dialog;
import java.awt.Event;
import java.awt.Menu;
import java.awt.MenuBar;

public class GameFrame extends Frame {
    protected Panel p;
    protected MenuItem newItem, abortItem;
    protected GameManager gm;
    protected int width, height;
    protected OptionsDialog d;
    
    public GameFrame(Applet app, int witdh, int height){
        super("Alien Landing");
        
        this.width = width;
        this.height = height;
        gm = (GameManager) app;
        
        setSize(width+13,height+65);
        MenuBar menubar = new MenuBar();
        Menu m1 = new Menu("Game");
        newItem = new MenuItem("New Game");
        m1.add(newItem);
        abortItem = new MenuItem("Abort Game");
        abortItem.setEnabled(false);
        m1.add(abortItem);
        m1.add(new MenuItem("Exit"));
        Menu m2 = new Menu("Options");
        m2.add(new MenuItem("Change Options ..."));
        menubar.add(m1);
        menubar.add(m2);
        setMenuBar(menubar);
        p = new Panel();
        p.setLayout(new BorderLayout());
        add("Center",p);
        
        setCursor(Cursor.CROSSHAIR_CURSOR);
        
        setVisible(true);
    }
    
    public void gameOver(){
        abortItem.setEnabled(false);
        newItem.setEnabled(true);
    }
    
    public boolean action(Event e, Object o){
        if(e.target instanceof MenuItem){
            String s = (String)o;
            if(e.target == newItem){
                gm.newGame();
                newItem.setEnabled(false);
                abortItem.setEnabled(true);
            } else if(e.target == abortItem){
                gm.gameOver();
            } else if(s.equals("Exit")){
                d.setVisible(false);
                gm.stop();
                gm.destroy();
                dispose();
            } else if(s.equals("Chnage Options...")){
                d = new OptionsDialog(this,gm);
                d.setVisible(true);
            }
            return true;
        } else 
            return false;
        
    }
    
    
}
