
package gui;

import java.awt.Button;
import java.awt.Event;

public class MyButton extends Button {
    public MyButton(String s){
        super(s);
    }
    
    public boolean action(Event e, Object o){
        System.out.println("(MyButton.action)Button Click!");
        if(e.target instanceof Button){
            return true;
        }
        return false;
    }
}
