
package gui;

import java.applet.Applet;

import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;

import java.awt.Event;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;


public class GuiTest extends Applet {
    public void init(){
        
        //flowLayout();
        //borderLayout();
        gridLayout();
        
    }
    
    private void flowLayout(){
        setLayout(new FlowLayout());
        //setLayout(new FlowLayout(FlowLayout.CENTER));
        //setLayout(new FlowLayout(FlowLayout.CENTER,17,33));
        
        Button b = new Button("Hello");
        add(b);
        
        Checkbox check = new Checkbox("check");
        add(check);
        
        CheckboxGroup vote = new CheckboxGroup();
        Checkbox gosling = new Checkbox("Gosling",vote,false);
        add(gosling);
        Checkbox clark = new Checkbox("Clark",vote,false);
        add(clark);
        Checkbox bird = new Checkbox("bird",vote,true);
        add(bird);
        
        String s = vote.getSelectedCheckbox().getLabel();
        //vote.setCurrent(clark);
        
        Label l1 = new Label("Label 1");
        add(l1);
        
        Label l2 = new Label("Label 1", Label.CENTER);
        l2.setAlignment(Label.RIGHT);
        add(l2);
        
        TextField t0 = new TextField();
        TextField t1 = new TextField(13);
        TextField t2 = new TextField("Sample text1");
        add(t0);
        add(t1);
    }
    
    private void borderLayout(){
        setLayout(new BorderLayout());
        //setLayout(new BorderLayout(17,13));
        
        Button b = new Button("Hello");
        add(b);
        
        add("North",b);
        //add("South",b);
        //add("East",b);
        //add("West",b);
        //add("Center",b);
        
    }
    
    private void gridLayout(){
        //setLayout(new GridLayout(16,16));
        setLayout(new GridLayout(2,4,13,13));
        
        Button b = new Button("Hello");
        add(b);
        
        Checkbox check = new Checkbox("check");
        add(check);
        
        CheckboxGroup vote = new CheckboxGroup();
        Checkbox gosling = new Checkbox("Gosling",vote,false);
        add(gosling);
        Checkbox clark = new Checkbox("Clark",vote,false);
        add(clark);
        Checkbox bird = new Checkbox("bird",vote,true);
        add(bird);
    
    }
    
    private void cardLayout(){
    
    }
    
    private void gribBagLayout(){
    
    }
    
    public boolean action(Event e, Object o){
        System.out.println("(GuiTest.action)");
        if(e.target instanceof Button){
            System.out.println("Button Click!");
            return true;
        }
        else if(e.target instanceof Checkbox){
            System.out.println("Checkbox Click!");
            return true;
        }
        return false;
    }
    
    /*public boolean handleEvent(Event e){
        if(e.id == Event.ACTION_EVENT){
            System.out.println("(GuiTest.handleEvent)Button Click!");
            if(e.target instanceof Button){
                return true;
            }
        }
        return false;
    }*/
}
