
package gui;

import java.applet.Applet;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Frame;

public class FrameExample extends Applet {
    public void init(){
        Frame f = new Frame("FrameTest");
        
        Dialog dialog = new Dialog(f, "A Dialog",false);
        dialog.show();
        
        f.resize(113, 117);
        f.setResizable(true);
        f.setCursor(Frame.CROSSHAIR_CURSOR);
        f.show();
    }
}
