
package gui;

import java.applet.Applet;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

public class PanelExample extends Applet {
    public void init(){
        setLayout(new GridLayout(2,3,13,13));
        Panel p = new Panel();
        
        p.setLayout(new FlowLayout(FlowLayout.RIGHT));
        
        p.add(new Button("Buttons"));
        p.add(new Button("in"));
        p.add(new Button("panel"));
        
        p.add(new Label("Label 1",Label.CENTER));
        p.add(new Label("Label 2",Label.CENTER));
        p.add(new Label("Label 3",Label.CENTER));
        p.add(new Label("Label 4",Label.CENTER));
        p.add(new Label("Label 5",Label.CENTER));
        
        add(p);
    }
    
}
