
package sprites;

import java.awt.Graphics;


public abstract class Sprite {
    protected boolean visible;
    protected boolean active;
    
    public abstract void paint(Graphics g);
    public abstract void update();
    
    public boolean isVisible(){
        return visible;
    }
    
    public void setVisible(boolean b){
        visible = b;
    }
    
    public boolean isActive(){
        return active;
    }
    
    public void setActive(boolean a){
        active = a;
    }
    
    public void suspend(){
        setVisible(false);
        setActive(false);
    }
    
    public void restore(){
        setVisible(true);
        setActive(true);
    }
}
