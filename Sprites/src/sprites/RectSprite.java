
package sprites;

import java.awt.Color;
import java.awt.Graphics;

public class RectSprite extends Sprite2D {
    
    protected int width, height;
    
    public RectSprite(int x, int y, int w, int h, Color c){
        locx = x;
        locy = y;
        width = w;
        height = h;
        color = c;
        fill = false;
        restore();
    }
    
    public RectSprite(int w, int h, Color c){
        locx = 0;
        locy = 0;
        width = w;
        height = h;
        color = c;
        restore();
    }

    @Override
    public void paint(Graphics g) {
        if(visible){
            g.setColor(color);
        
            if(fill){
                g.fillRect(locx, locy, width, height);
            } else{
                g.drawRect(locx, locy, width, height);
            }
        }
    }

    @Override
    public void update() {
        
    }
    
}
