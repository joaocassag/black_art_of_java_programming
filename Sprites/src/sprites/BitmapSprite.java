
package sprites;

import AlienLandingGame.Intersect;
import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;

public class BitmapSprite extends Sprite {
    
    protected int locx;
    protected int locy;
    
    protected int width, height;
    
    public Image image;
    public Applet applet;
    
    public BitmapSprite(int x, int y, Image i, Applet a){
        locx = x;
        locy = y;
        image = i;
        applet =a;
        if(image !=null){
            width = image.getWidth(a);
            height = image.getHeight(a);
        }
        restore();
    }
    
    public BitmapSprite(Image i, Applet a){
        locx = 0;
        locy = 0;
        image = i;
        applet =a;
        if(image !=null){
            width = image.getWidth(a);
            height = image.getHeight(a);
        }
        restore();
    }
    
    public void setSize(int w, int h){
        width = w;
        height = h;
    }
    
    public void update(){
    
    }

    @Override
    public void paint(Graphics g) {
        if(visible){
            g.drawImage(image, locx, locy, applet);
        }
    }
}
