
package sprites;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class Bounce extends Applet implements Runnable {
    Thread animation;
    Graphics sc;
    Image image;
    
    static final int NUM_SPRITES = 3;
    static final int REFRESH_RATE = 80;
    
    Sprite sprites[];
    int width, height;
    
    public void init(){
        System.out.println(">> init <<");
        setBackground(Color.black);
        width = getBounds().height;
        height= getBounds().height;
        initSprites();
        image = createImage(width, height);
        sc = image.getGraphics();
    }

    @Override
    public void run() {
       while(true){
           repaint();
           updateSprites();
           try{ Thread.sleep(REFRESH_RATE);
           }catch(Exception e){}
       }
    }

    private void initSprites() {
        sprites = new Sprite[NUM_SPRITES];
        //sprites[0] = new BouncingBitmap(37,37,
        //                                    getImage(getCodeBase(),"image.gif"),this, 
        //                                    width-1, height-1);  //BitmapSprite defined as lower priority - will appear behid everything else
        sprites[0] = new RectSprite(0,0,width-1, height-1, Color.green);
        sprites[1] = new BouncingRect(0,0,30, 30, Color.yellow, width-1, height-1);
        sprites[2] = new BouncingRect(17,17,13,13, Color.red, width-1, height-1);
        
        
        ((Moveable)sprites[1]).setVelocity(4,3);
        ((Moveable)sprites[2]).setVelocity(1,2);
        ((Sprite2D)sprites[2]).setFill(true);
        //((Moveable)sprites[0]).setVelocity(1,3);
        //((BitmapSprite)sprites[0]).setSize(144,113);
    }
    
    public void start(){
        System.out.println(">> start <<");
        animation = new Thread(this);
        if(animation != null){
            animation.start();
        }
    }
    
    public void stop(){
        System.out.println(">> stop <<");
        if(animation !=null){
            animation.stop();
            animation = null;
        }
    }
    
    public void updateSprites(){
        for(int i =0; i<sprites.length; i++){
            sprites[i].update();
        }
    }
    
    /*public void update(Graphics g){
        
    }*/
    
    public void paint(Graphics g){
        sc.setColor(Color.black);
        sc.fillRect(0, 0, width, height);
        for(int i=0;i<sprites.length;i++){
            sprites[i].paint(sc);
        }
        g.drawImage(image, 0, 0, this);
    }
    
}
