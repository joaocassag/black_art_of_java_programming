
package sprites;

import java.awt.Color;


public abstract class Sprite2D extends Sprite {
    
    public int locx;
    public int locy;
    
    Color color;
    public boolean fill;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }
    
}
