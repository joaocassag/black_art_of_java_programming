
package sprites;

import java.applet.Applet;
import java.awt.Image;

public class BouncingBitmap extends BitmapSprite implements Moveable {
    
    protected int max_width, max_height;
    
    protected int vx, vy;

    public BouncingBitmap(int x, int y, Image i, Applet a, int max_w, int max_h) {
        super(x, y, i, a);
        max_width = max_w;
        max_height = max_h;
    }

    @Override
    public void setPosition(int x, int y) {
        locx = x;
        locy = y;
    }

    @Override
    public void setVelocity(int x, int y) {
        vx = x;
        vy = y;
    }

    @Override
    public void updatePosition() {
        locx+=vx;
        locy+=vy;
    }
    
    public void update(){
        if((locx + width > max_width) || locx<0){
            vx = -vx;
        }
        
        if((locy + height > max_height) || locy<0){
            vy = -vy;
        }
        updatePosition();
    }
    
}
