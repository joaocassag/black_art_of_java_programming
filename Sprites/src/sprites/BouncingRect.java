
package sprites;

import java.awt.Color;

public class BouncingRect extends RectSprite implements Moveable {
    
    protected int max_width;
    protected int max_height;
    
    protected int vx;
    protected int vy;

    public BouncingRect(int x, int y, int w, int h, Color c, int max_w, int max_h){
        super(x, y, w, h, c);
        max_width = max_w;
        max_height = max_h;
    }

    @Override
    public void setPosition(int x, int y) {
        locx = x;
        locy = y;
    }

    @Override
    public void setVelocity(int x, int y) {
        vx = x;
        vy = y;
    }

    @Override
    public void updatePosition() {
        locx+=vx;
        locy+=vy;
    }
    
    public void update(){
        if((locx + width > max_width) || locx<0){
            vx = -vx;
        }
        
        if((locy + height > max_height) || locy<0){
            vy = -vy;
        }
        updatePosition();
    }
    
}
