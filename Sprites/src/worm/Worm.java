
package worm;

import java.awt.Color;
import java.awt.Dimension;

public class Worm {
    Dimension m_points[];
    int m_startidx, m_endidx;
    int m_curridx;
    int m_startx, m_starty;
    int m_wormsize=4;
    int m_wormlen=5;
    int m_startxdir, m_startydir;
    int m_regrow=0;
    boolean m_nextstop = false, m_lastdropped = false;
    boolean m_nextadded = false;
    Dimension dDir;
    Dimension dCurr;
    Dimension dNext;
    Dimension dNewpoint;
    Dimension dOldpoint;
    Color wormcolor;
    
    public Worm(int startx, int starty, int wormlen, Color wclr, int wormsize, int initx, int inity){
        wormcolor = wclr;
        m_wormsize = wormsize;
        m_wormlen = wormlen;
        m_startxdir = initx;
        m_startydir = inity;
        
        m_points = new Dimension[m_wormsize];
        for(int i=0;i<m_points.length;i++)
            m_points[i] = new Dimension();
        m_startx = startx;
        m_starty = starty;
        
        dDir =  new Dimension();
        dCurr = new Dimension();
        dNext = new Dimension();
        dNewpoint = new Dimension();
        dOldpoint = new Dimension();
    }
    
    public void ReviveWorm(){
        int count, i;
        for(i=0,count=0;i<m_wormlen;i++,count++){
            m_points[count].width = i + m_startx;
            m_points[count].height = i + m_starty;
        }
        
        m_startidx = count -1;
        m_endidx = 0;
        m_regrow = 0;
        
        dDir.width = m_startxdir;
        dDir.height = m_startydir;
    }
    
    public boolean DoYouHaveSegmentAt(Dimension newpoint){
        int curridx = m_endidx;
        while(true){
            if(newpoint.width == m_points[curridx].width &&
               newpoint.height == m_points[curridx].height)
                return true;
            if(curridx == m_startidx)
                break;
            curridx++;
            if(curridx>=m_wormsize)
                curridx = 0;
        }
        return false;
    }
    
    public Dimension GetNextPoint(){
        dNext.width = m_points[m_startidx].width;
        dNext.height = m_points[m_startidx].height;
        
        dNext.width +=dDir.width;
        dNext.height +=dDir.height;
        
        m_nextadded = false;
        return dNext;
    }
    
    public boolean SetNextPoint(Dimension newpoint){
        int tempidx;
        m_startidx++;
        if(m_startidx>=m_wormsize)
            m_startidx = 0;
        m_points[m_startidx].width = dNewpoint.width = newpoint.width;
        m_points[m_startidx].height = dNewpoint.height = newpoint.height;
        
        tempidx = m_startidx+1;
        if(tempidx>=m_wormsize)
            tempidx = 0;
        
        m_nextadded = true;
        return tempidx == m_endidx;
    }
    
    public boolean DropLastPoint(){
        dOldpoint.width = m_points[m_endidx].width;
        dOldpoint.height = m_points[m_endidx].height;
        
        if(m_regrow==0)
            if(CountSegments()<=m_wormlen/2)
                m_regrow = 150;
        if(m_endidx == m_startidx){
            m_lastdropped = false;
            return m_lastdropped;
        }
        
        m_endidx++;
        if(m_endidx >= m_wormsize)
            m_endidx = 0;
        
        m_lastdropped = true;
        return m_lastdropped;
    }
    
    public boolean IsWormFirst(){
        m_curridx = m_endidx;
        m_nextstop = false;
        dCurr = m_points[m_curridx];
        return GetWormElement();
    }
    
    public boolean IsWormNext(){
        return GetWormElement();
    }
    
    public Dimension GetCurrentWormSegment(){
        return dCurr;
    }
    
    public Color GetCurrentWormColor(){
        return wormcolor;
    }
    
    public boolean GetWormElement(){
        if(m_nextstop)
            return false;
        
        if(m_curridx == m_startidx)
            m_nextstop = true;
        
        dCurr = m_points[m_curridx++];
        if(m_curridx>=m_wormsize)
            m_curridx = 0;
        return true;
    }
    
    public void SetNewDirectionFromKeyboard(int key){
        //1004 =up,1005=down, 1006=left, 1007=right 
        if(key ==1004 || key=='u'){
            if(dDir.height==0){
                dDir.width=0;
                dDir.height=-1;
            }
        }
        
        else if(key ==1005 || key=='j'){
            if(dDir.height==0){
                dDir.width=0;
                dDir.height=1;
            }
        }
        
        else if(key ==1006 || key=='h'){
            if(dDir.width==0){
                dDir.width=-1;
                dDir.height=0;
            }
        }
        
        else if(key ==1007 || key=='k'){
            if(dDir.width==0){
                dDir.width=1;
                dDir.height=0;
            }
        }
    }
    
    public void SetNewDirectionFromMouse(int wormscale, int x, int y){
        if(dDir.width==0){
            if(x>m_points[m_startidx].width*wormscale)
                dDir.width = 1;
            else
                dDir.width = -1;
            dDir.height=0;
        } else{
            if(y>m_points[m_startidx].height*wormscale)
                dDir.height = 1;
            else
                dDir.height = -1;
            dDir.width=0;
        }
    }

    private int CountSegments() {
        //TODO implement
        return 0;
    }
    
    public void SetDirection(int x, int y){
        dDir.width = x;
        dDir.height = y;
    }
    
    public Dimension GetDirection(){
        return dDir;
    }

    public boolean NotRegrowing() {
        if(m_regrow>0){
            m_regrow--;
            if((m_regrow<100) && (m_regrow & 7)==0)
                return false;
        }
        return true;
    }
    
}
