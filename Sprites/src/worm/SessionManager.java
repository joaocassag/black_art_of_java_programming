
package worm;

import java.awt.Color;
import java.awt.Dimension;

public class SessionManager {
    static int SIZEX = 30;
    static int SIZEY = 30;
    static int WORMCOUNT=3;
    
    boolean m_newtr = false;
    boolean m_showscore = false, m_dontdrop = false;
    boolean m_speedup = false;
    
    Dimension wMax = new Dimension(SIZEX,SIZEY);
    Dimension dTemp = new Dimension();
    Dimension treatpoint = new Dimension();
    Dimension collisionpoint = new Dimension();
    Dimension newpoint = new Dimension();
    Dimension autopoint = new Dimension();
    Dimension dpoint[] = new Dimension[4];
    
    Worm worm[];
    int currscore;
    int highscore;
    int lastscore;
    int treatcount;
    int forcedelay;
    int currautoworm;
    int adddropidx;
    int grow;
    long age;
    int m_gatallidx;
    int dirchange;
    
    public SessionManager(){
        currscore = lastscore = highscore = 0;
        for(int i =0;i<4;i++)
            dpoint[i] = new Dimension();
        
        worm = new Worm[WORMCOUNT];
        worm[0] = new Worm(2,5,10,Color.blue,SIZEX*SIZEY/2,1,0);
        worm[1] = new Worm(2,15,20,Color.yellow,22,0,-1);
        worm[2] = new Worm(2,1,20,Color.cyan,22,0,2);
        
        Restart();
    }

    public void Restart() {
        collisionpoint.width =-1;
        collisionpoint.height=-1;
        
        lastscore = currscore;
        currscore =0;
        treatcount = 0;
        currautoworm = 1;
        grow = 0;
        age = 0;
        forcedelay = 0;
        dirchange = 0;
        
        for(int i=0; i<WORMCOUNT;i++)
            worm[i].ReviveWorm();
        NewTreat(true);
    }

    private void NewTreat(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean Next(){
        boolean forcedrop;
        m_newtr = false;
        m_showscore = false;
        
        newpoint = worm[0].GetNextPoint();
        
        if(!SearchForTreat(newpoint)){
            if(SearchForCollision(newpoint))
                return true;
        }else{
            forcedelay = 300;
            m_newtr = true;
        }
        
        forcedrop = worm[0].SetNextPoint(newpoint);
        
        if((age++%32) !=0){
            if(!forcedrop){
                if(grow==0)
                    forcedrop = true;
                else
                    grow--;
            }
        }else{
            if(treatcount!=0)
                currscore = treatcount*(int)(age/10);
            else
                currscore = 0;
            
            if(currscore>highscore)
                highscore = currscore;
            
            m_showscore = true;
        }
        
        if(m_dontdrop ==false)
            forcedrop=true;
        
        if(forcedrop)
            worm[0].DropLastPoint();
        
        if(m_newtr)
            NewTreat(false);
        
        if(currautoworm!=0)
            DoAutoWorms();
        
        return false;
    }

    private void DoAutoWorms() {
        int direction, dx, dy;
        
        currautoworm++;
        if(currautoworm>=WORMCOUNT)
            currautoworm=1;
        
        //save the current direction of motion
        dpoint[0] = worm[currautoworm].GetDirection();
        
        autopoint = worm[currautoworm].GetNextPoint();
        
        if(dirchange>0)
            dirchange--;
        
        //do different things depending on what we hit
        //if hit a wall, go randomly right or left
        if(HitWall(autopoint) == true || dirchange ==1){
            dirchange = (int)(Math.random()+1)*5+10;
            
            //pick a direction at random
            if(Math.random()<0)
                direction = -1;
            else
                direction = 1;
            
            dpoint[1] = GetNewDirection(dpoint[0],direction);
        }
        else if(HaveSeg(autopoint)==true || HitTreat(autopoint) ==true){
            //always try to go left if hit an object
            dpoint[1]= GetNewDirection(dpoint[0],-1);
        }
        else{
            //no collision, all done
            worm[currautoworm].SetNextPoint(autopoint);
            //extend worm if it shrunk
            if(worm[currautoworm].NotRegrowing())
                worm[currautoworm].DropLastPoint();
            return;
        }
        
        //create remaining directions
        if(dpoint[0].width == 1){
            dpoint[2].width = -1;
            dpoint[2].height=0;
        }
        
        else if(dpoint[0].width == -1){
            dpoint[2].width = 1;
            dpoint[2].height=0;
        }
        else{
            dpoint[2].width = 0;
            dpoint[2].height=1;
        }
        
        if(dpoint[1].width == 1){
            dpoint[3].width = -1;
            dpoint[3].height=0;
        }
        
        else if(dpoint[1].width == -1){
            dpoint[3].width = 1;
            dpoint[3].height=0;
        }
        
        else if(dpoint[1].height == 1){
            dpoint[3].width = 0;
            dpoint[3].height=-1;
        }
        else{
            dpoint[3].width = 0;
            dpoint[3].height=1;
        }
        
        //skip this first since it is a known collision
        for(int i =1;i<4;i++){
            worm[currautoworm].SetDirection(dpoint[i].width, dpoint[i].height);
            autopoint = worm[currautoworm].GetNextPoint();
            
            if(HitWall(autopoint) == false && HaveSeg(autopoint) == false && HitTreat(autopoint) ==false){
                //no collusion, all done
                worm[currautoworm].SetNextPoint(autopoint);
                //extend worm if it shrunk
                if(worm[currautoworm].NotRegrowing())
                    worm[currautoworm].DropLastPoint();
                return;
            }
        }
        
        //no places left to go!
        if(worm[currautoworm].NotRegrowing())
            worm[currautoworm].DropLastPoint();
    }

    private boolean SearchForTreat(Dimension newpoint) {
        if(newpoint.width!=treatpoint.width || newpoint.height!= treatpoint.height)
            return false;
        
        treatcount++;
        grow = (int)(age/100)+2;
        return true;
    }

    private boolean SearchForCollision(Dimension newpoint) {
        if(HaveSeg(newpoint)){
            collisionpoint.width = newpoint.width;
            collisionpoint.height = newpoint.height;
            return true;
        }
        if(HitWall(newpoint)){
            collisionpoint.width = -1;
            collisionpoint.height = -1;
            return true;
        }
        return false;
    }

    private boolean HaveSeg(Dimension segpoint) {
        for(int i =0;i<WORMCOUNT;i++)
            if(worm[0].DoYouHaveSegmentAt(segpoint))
                return true;
        return false;
    }

    private boolean HitWall(Dimension segpoint) {
        if(segpoint.width<0 || segpoint.height<0 || segpoint.width>=wMax.width || segpoint.height>=wMax.height)
            return true;
        return false;
    }

    boolean IsNewTreat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    boolean IsFirstDropped() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    Dimension CurrDroppedPoint(){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    boolean IsDisplayScore() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    int GetPixelWidth(int WORMSCALE) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    int GetPixelHeight(int WORMSCALE) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    int GetDelay() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void SetDirection(Worm w,int x, int y){
        w.SetDirection(x, y);
    }
    
    public Dimension GetDirection(Worm w){
        return w.GetDirection();
    }

    private Dimension GetNewDirection(Dimension olddir, int motion) {
        if(olddir.width==1 || olddir.width == -1){
            dTemp.width = 0;
            dTemp.height = motion;
        }
        else{
            dTemp.width = motion;
            dTemp.height = 0;
        }
        return dTemp;
    }

    private boolean HitTreat(Dimension autopoint) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
