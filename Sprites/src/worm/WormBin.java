
package worm;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageProducer;

public class WormBin extends Applet implements Runnable {
    
    SessionManager smgr;
    AudioClip hitsoundData;
    AudioClip atesoundData;
    
    Thread kicker;
    private int BORDERPAD;
    private int WORMSCALE;
    private Image wormimage;
    private Graphics wormgraphics;
    
    public void init(){
    
    }
    
    public boolean Next(Graphics g){
        if(smgr.Next()){
            if(hitsoundData!=null)
                hitsoundData.play();
            return true;
        }
        
        if(smgr.IsNewTreat()){
            if(atesoundData!=null)
                atesoundData.play();
            try{Thread.sleep(100);}catch(Exception e){}
            DrawTreat(g);
        }
        
        if(smgr.IsFirstDropped()){
            do{
                g.setColor(Color.black);
                DrawSegment(g,smgr.CurrDroppedPoint());
            }while(smgr.IsFirstDropped());
        }
        
        if(smgr.IsDisplayScore())
            WriteScore();
        
        return false;
    }

    @Override
    public void run() {
        int delay;
        if(kicker!=null){
            //create a hidden buffer and clear it
            wormimage = createImage(smgr.GetPixelWidth(WORMSCALE)+BORDERPAD,smgr.GetPixelHeight(WORMSCALE)+BORDERPAD);
            
            //getcontext
            wormgraphics = wormimage.getGraphics();
            wormgraphics.setFont(getFont());
            wormgraphics.setColor(Color.black);
            wormgraphics.fillRect(0, 0,smgr.GetPixelWidth(WORMSCALE)+BORDERPAD,smgr.GetPixelHeight(WORMSCALE)+BORDERPAD);
            //reset all variables to beginning, create a new form
            Reset();
            
            //draw current game status and paint to applet
            //client area
            PaintForm(wormgraphics,false);
            repaint();
            
            ThreadPause("Start");
            
            //loop until system terminates us
            while(kicker!=null){
                while(size().width>0 && size().height>0 && kicker!=null){
                    if(Next(wormgraphics )){
                        //worm collided, redraw all new entities
                        PaintForm(wormgraphics,true);
                        repaint();
                        
                        //stop game until users presses mouse or hits a key
                        ThreadPause("Start");
                        
                        //reset all variables to beginning, create a new form
                        Reset();
                        
                        //draw current game status and paint to applet
                        //client area
                        PaintForm(wormgraphics,false);
                        repaint();
                        
                        break;
                    }
                    //repaint from hidden area to visible area
                    repaint();
                    
                    //sleep for a very short time to make the game playable on fast computers
                    delay = smgr.GetDelay();
                    if(delay>0)
                        try{Thread.sleep(delay);}catch(Exception e){}
                }
            }
        }
    }

    private void DrawTreat(Graphics g) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void DrawSegment(Graphics g, Dimension CurrDroppedPoint) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void WriteScore() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void Reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void PaintForm(Graphics wormgraphics, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void ThreadPause(String start) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
