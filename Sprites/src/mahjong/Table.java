
package mahjong;

public class Table {
    public static Table[] global = new Table[25];
    int id =-1;
    Player[] players = new Player[8];
    
    public Table() throws Exception{
        synchronized(global){
            for(int i =0;i<global.length;i++){
                if(global[i]==null){
                    id = i;
                    global[i]=this;
                    break;
                }
            }
        }
        if(id==-1) throw new Exception("Table exception");
    }
    
    public synchronized void outputTable(byte[] p){
        for(int i =0;i<players.length;i++){
            if(players[i]!=null)
                players[i].output(p);
        }
    }
    
    public synchronized void join(Player him, boolean play){
        int s0 = play? 0 : 4;
        int s1 = play? 4 : players.length;
        for(int i =s0;i<s1;i++){
            if(players[i]==null){
                players[i]= him;
                him.table = this;
                him.seat = i;
                Player.outputAll(Packet.SPTablePlayer(id, i, him.id));
                break;
            }
        }
        him.output(Packet.Message(Packet.MSG_GOD, 0, "Table is Full "));
        return;
    }
    
    public void leave(Player him){
        synchronized(global){
            synchronized(this){
                if(him.table!=this) return;
                Player.outputAll(Packet.SPTablePlayer(id, him.seat, -1));
                players[him.seat] = null;
                him.table = null;
                him.seat=-1;
                for(int i=0;i<4;i++)
                    if(players[i]!=null)
                        return;
                for(int i=0;i<players.length;i++){
                    if(players[i]!=null){
                        Player.outputAll(Packet.SPTablePlayer(id, i, -1));
                        players[i].table = null;
                        players[i].seat = -1;
                        players[i]=null;
                    }
                }
                global[id]=null;
            }
        }
    }
    
    public synchronized void update(Player him){
        for(int i=0;i<players.length;i++){
            if(players[i]!=null)
             him.output(Packet.SPTablePlayer(id, i,t.players[i].id));
        }
    }
}
