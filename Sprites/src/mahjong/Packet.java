
package mahjong;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;

public class Packet extends DataOutputStream {
    ByteArrayOutputStream b_out;
    
    public Packet(ByteArrayOutputStream buf) {
        super(buf);
        b_out=buf;
    }
    
    public byte[] buf(){
        return b_out.toByteArray();
    }
    
    public static Packet New(int size){
        ByteArrayOutputStream buf = new ByteArrayOutputStream(size);
        return new Packet(buf);
    }
    
    public static final byte SP_SOME_PACKET=0;
    public static byte[] SPSomePacket(byte b, int i, String s){
        try{
            Packet p = New(0);
            p.writeByte(SP_SOME_PACKET);
            p.writeByte(b);
            p.writeInt(i);
            p.writeUTF(s);
            return p.buf();
        }catch(Exception e){return null;}
    }
}
