
package mahjong;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Event;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Greeting extends Applet {
    Button main_button = new Button("Connect NOW!");
    Client main = null;
    
    public void init(){
        add(main_button);
        show();
    }
    
    public void restart(){
        main = null;
        main_button.setLabel("Conenct again");
        main_button.enable();
    }
    
    public synchronized boolean action(Event evt, Object what){
        if(evt.target ==main_button){
            main_button.disable();
            try{
                Socket s = new Socket(getParameter("ServerHost"),5656);
                DataInputStream i = new DataInputStream(s.getInputStream());
                DataOutputStream o = new DataOutputStream(s.getOutputStream());
                main_button.setLabel("Connected!");
                main = new Client(this, s, i, o);;
            }catch(Exception e){restart();}
        }
        return true;
    }
}
