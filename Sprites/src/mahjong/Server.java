
package mahjong;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server{
    public static void main(String args[]){
        ServerSocket main_sock = null;
        try{
            main_sock = new ServerSocket(5656);
        }catch(Exception e){
            System.out.println("Failed to open socket connection");
            System.exit(1);
        }
        while(true){
            try{
                Socket s = main_sock.accept();
                DataInputStream i = new DataInputStream(s.getInputStream());
                DataOutputStream o = new DataOutputStream(s.getOutputStream());
                (new Player(s,i,o)).start();
            }catch(Exception e){}
        }
    }
}
