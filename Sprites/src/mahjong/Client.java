
package mahjong;

import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.TextField;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client extends Frame implements Runnable {
    Greeting greeter;
    Socket sock;
    DataInputStream in;
    DataOutputStream out;
    Thread listener;
    CPlayer[] g_players = new CPlayer[100];
    CTable[] g_tables = new CTable[25];
    int myID=-1;
    
    TextField login_name;
    
    public Client(Greeting g, Socket s, DataInputStream i, DataOutputStream o){
        sock = s;
        in = i;
        out = o;
        greeter = g;
        (listener = new Thread(this)).start();
        
        login_name = new TextField(20);
        setLayout(new FlowLayout());
        add(login_name);
        pack();
        show();
    }
    
    public synchronized boolean action(Event evt, Object what){
        if(evt.target == login_name){
            login_name.setEditable(false);
            output(Packet.CPLogin(login_name.getText()));
        }
        return true;
    }
    
    public void hdLogin() throws Exception{
        byte result = in.readByte();
        if(result!=Packet.LOGIN_OK){
            login_name.setEditable(true);
            login_name.setText("");
            return;
        }else{
            after_login();
        }
        
    }
    
    public void output(byte[] p){
        if(p!=null && p.length>0){
            try{
                out.write(p,0,p.length);
            }catch(Exception e){
                disconnect();
                listener.stop();
                dispose();
                greeter.restart();
            }
        }
    }
    
    public void disconnect(){
        try{
            in.close();
            out.close();
            sock.close();
        }catch(Exception e){}
    }

    @Override
    public void run(){
        //TODO
        while(true){
            try{
                byte tag = in.readByte();
                switch(tag){
                    case SP_SOME_PACKET:
                        hdSomePacket();
                        break;
                    default:
                        //;
                }
            }catch(Exception e){break;}
        }
        disconnect();
        dispose();
        greeter.restart();
    }
    
    public void hdYouAre() throws Exception{
        myID=(int)in.readByte();
    }
    
    public void hdSomePacket() throws Exception{
        byte b = in.readByte();
        int i = in.readInt();
        String s = in.readUTF();
        //TODO
    }
    
}
