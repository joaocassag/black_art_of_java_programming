
package mahjong;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Player extends Thread {
    public static Player[] global = new Player[100];
    public int id =-1;
    public String name = null;
    private Socket sock;
    private DataInputStream in;
    private DataOutputStream out;
    public boolean connected = true;
    private PlayerOutput out_thread = null;
    public Table table = null;
    public int seat = -1;
    
    public Player(Socket s, DataInputStream in, DataOutputStream out) throws Exception{
        synchronized(global){
            for(int i =0;i<global.length;i++){
                if(global[i]==null){
                    id=i;
                    global[i]=this;
                    break;
                }
            }
        }
        if(id==-1) throw new Exception("no connections available");
        sock = s;
        this.in = in;
        this.out = out;
        (out_thread = new PlayerOutput(this,out)).start();
        output(Packet.SPYouAre(id));//notify the player
    }
    
    public void output(byte[] p){
        if(connected) out_thread.output(p);
    }
    
    public static void outputAll(byte[] p){
        synchronized(global){
            for(int i=0;i<global.length;i++){
                Player him = global[i];
                if(him!=null && him.name !=null)
                    him.output(p);
            }
        }
    }
    
    public synchronized void disconnect(){
        if(id ==-1) return;
        connected=false;
        if(table!=null)
            table.leave(this);
        synchronized(global){
            global[id]=null;
        }
        try{
            in.close();
            out.close();
            sock.close();
        }catch(Exception e){}
        if(name!=null)
            outputAll(Packet.SPLeave(id));
        id=-1;
    }
    
    private Integer working = new Integer(0);
    public void run(){
        while(connected){
            try{
                byte tag = in.readByte();
                synchronized(working){
                    switch(tag){
                        case CP_QUIT:
                            break;
                        case CP_MESSAGE:
                            hdMessage();
                            break;
                    }
                }
            }catch(Exception e){
                break;
            }
        }
        connected = false;
        out_thread.stop();
        disconnect();
    }
    
    public void stopOnWait(){
        connected = false;
        synchronized(working){
            stop();
        }
    }
    
    public void hdMessage() throws Exception{
        byte type = in.readByte();
        String text = in.readUTF();
        if(type == Packet.MSG_SERVER)
            System.out.println("Msg from "+id+": "+text);
        else if (type == Packet.MSG_TABLE && table!=null)
            table.outputTable(Packet.SPMessage(Packet.MSG_TABLE,id,text));
        else
            outputAll(Packet.SPMessage(Packet.MSG_ALL,id,text));
    }
    
    public void hdLogin() throws Exception{
        String n = in.readUTF();
        synchronized(global){
            for(int i=0;i<global.length;i++){
                if(global[i]!=null && n.equals(global[i].name)){
                    output(Packet.SPLogin(Packet.LOGIN_TWICE));
                    return; //reject login
                }
            }
            name = n;//accept login
        }
        output(Packet.SPLogin(Packet.LOGIN_OK));
        
        synchronized(global){
            for(int i=0;i<global.length;i++){
                if(global[i]!=null && global[i].name !=null)
                    output(Packet.SPJoin(i,global[i].name));
            }
        }
        
        //send updates to all tables
        synchronized(Table.global){
            for(int i=0;i<Table.global.length;i++){
                if(Table.global[i]!=null){
                    output(Packet.SPTableOpt(i,null));
                    Table.global[i].update(this);
                }
            }
        }
    }
}
