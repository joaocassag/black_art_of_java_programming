
package AlienLandingGame;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class GunManager {
    
    private GunSprite gun;
    private int gun_width;
    private int gun_height;
    private MissileSprite missile;
    private int min_x, max_x;
    
    private int gun_min_x, gun_max_x;
    private int mis_min_x, mis_max_x;
    private int gun_y;
    private boolean displayHit;
    private int energy;
    
    private int maxEnergy;
    private int energyDec;
    
    private GameManager game;
    static int width, height;
    
    static final int ENERGY_PER_HIT = 5;
    
    static final int MISSILE_WIDTH=3;
    static final int MISSILE_HEIGHT=27;
    static final int MISSILE_SPEED=-27;
    static final Color MISSILE_COLOR=Color.red;
    
    String energyString="Energy";
    
    public GunManager(int maxEnergy, int energyDec,int w, int h,
                        Image gunImage, Intersect target[], Applet a){
        this.maxEnergy = maxEnergy;
        this.energyDec = energyDec;
        width = w;
        height = h;
        gun = new GunSprite(gunImage,a,this);
        
        gun_width = gunImage.getWidth(a)/2;
        gun_height = gunImage.getHeight(a);
        
        gun_y = height - gun_height;
        min_x = gun_width;
        max_x = width - gun_width;
        gun_min_x = 0;
        gun_max_x = width - 2*gun_width;
        mis_min_x = min_x-2;
        mis_max_x = max_x-2;
        gun.setPosition(width/2-gun_width, gun_y);
        missile = new MissileSprite(MISSILE_WIDTH,MISSILE_HEIGHT,
                                    MISSILE_COLOR,MISSILE_SPEED,
                                    height-gun_height+13,0,target);
        game = (GameManager) a;
    }
    
    public void moveGun(int x){
        if(x<=min_x){
            gun.setPosition(gun_min_x, gun_y);
        } else if(x>=max_x){
            gun.setPosition(gun_max_x, gun_y);
        } else{
            gun.setPosition(x-gun_width, gun_y);
        }
    }
    
    public void fireMissile(int x){
        if(!missile.isActive()){
            if(x<=min_x){
                missile.init(mis_min_x);
            } else if(x>=max_x){
                missile.init(mis_max_x);
            } else{
                missile.init(x-2);
            }
        }
    }
    
    public void update(){
        missile.update();
    }
    
    public void paint(Graphics g){
        if(displayHit){
            g.setColor(Color.red);
            
            g.fillRect(0, gun_y, width, gun_height);
            displayHit = false;
        } else{gun.paint(g);}
        missile.paint(g);
        
        g.setColor(Color.red);
        g.drawString(energyString, 3, 13);
        g.fillRect(0, 17, energy, 10);
    }
    
    public GunSprite getGun(){
        return gun;
    }
    
    public int getGunY(){
        return gun_y;
    }
    
    public void handleHit(){
        displayHit = true;
        energy -= energyDec;
        if(energy <= 0){
            game.gameOver();
            gun.suspend();
            missile.suspend();
        }
    }
    
    public void gameOver(){
    
    }
    
    public void newGame(){
        gun.setPosition(width/2-gun_width, gun_y);
        gun.restore();
        displayHit = false;
        energy = maxEnergy;
    }

    void setEnergyDec(int energyDec) {
        this.energyDec = energyDec;
    }
}
