
package AlienLandingGame;

import gui.GameFrame;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;

public class GameManager extends Applet implements Runnable {
    
    static final int REFRESH_RATE = 80;
    Thread animation;
    Graphics sc;
    Image image;
    
    static final int UFO_VALUE = 130;
    static final int MAX_LANDED = 5;
    static final int MAX_LEVEL = 9;
    static final int MAX_ENERGY = 113;
    
    private int score;
    private int numLanded;
    
    static final int NUM_UFOs=6;
    
    Image ufoImages[] = new Image[NUM_UFOs];
    Image attackImages[] = new Image[6];
    protected Image explodeImages[] = new Image[4];
    Image  gunImage;
    GunManager gm;
    UFOManager um;
    
    private boolean playing;
    private int screen;
    static final int INTRO = 0;
    static final int GAME_OVER = 1;
    
    AudioClip expSound;
    
    Font smallFont = new Font("Helvetica", Font.BOLD,12);
    Font mediumFont = new Font("Helvetica", Font.BOLD,14);
    Font bigFont = new Font("Helvetica", Font.BOLD,18);
    
    FontMetrics fm;
    
    public int startLevel;
    public int energyDec;
    public boolean sound;
    
    int width, height;
    
    String scoreString = "Score: ";
    String ufoLandedString ="UFOs Landed: ";
    String gameOverString = "   GAME OVER  ";
    String clickString = "Shift-Click to Continue";
    String alienLandingString = "Alien Landing";
    int stringWidth;
    String introString[] = new String[8];
    
    GameFrame f;
    
    public void init(){
        System.out.println(">> GameManager.init <<");
        //showStatus("Loading Images -- WAIT!");
        setBackground(Color.black);
        width = 240;//width=700;//width = getBounds().width;//350
        height = 270;//height=400;//height = getBounds().height;//200
        resize(width,height);
        startLevel = 2;
        energyDec = 5;
        sound = false;
        //setBounds(0, 0, width, height);
        f = new GameFrame(this,width,height);
        
        loadImages();
        
        try{expSound = getAudioClip(getCodeBase(),"ufo_explosion_sound.au");} 
        catch(Exception e){System.out.println(" Failed to load sound!");}
        
        um = new UFOManager(startLevel,MAX_LEVEL,width, height,
                ufoImages,attackImages, explodeImages,this,expSound);
        gm = new GunManager(MAX_ENERGY,energyDec,width, height, gunImage,um.getUFO(),this);
        um.initialize(gm);
        
        playing = false;
        screen = INTRO;
        
        image = createImage(width,height);
        sc = image.getGraphics();
        sc.setFont(bigFont);
        fm = sc.getFontMetrics(bigFont);
        stringWidth = fm.stringWidth(alienLandingString);
        
        introString[0] = "You are Humanity's last hope!";
        introString[1] = "Destroy the green Alien Landers";
        introString[2] = "by using the Mouse to Move";
        introString[3] = "your Missile Launcher. Click";
        introString[4] = "to Fire Missile. if 5 Aliens";
        introString[5] = "Land, or Energy Runs Out,";
        introString[6] = "HUmans will be Eaten Alive!";
        introString[7] = "Click to start the Game";
    }
    
    public void loadImages(){
        try{
            System.out.println(">> GameManager.loadImages <<");
            MediaTracker t = new MediaTracker(this);
            //System.out.println("path for photos is: "+getCodeBase().getPath());
            System.out.println("Loading gun image");
            gunImage = getImage(getCodeBase(),"single_gun.gif");
            System.out.println("Add gun image");
            t.addImage(gunImage,0);
            for(int i=0; i<NUM_UFOs;i++){
                System.out.println("Loading ufo image "+i);
                ufoImages[i] = getImage(getCodeBase(),"ufo"+i+".gif");
                System.out.println("Add ufo image "+i);
                t.addImage(ufoImages[i],0);
            }
            
            for(int i=0; i<attackImages.length;i++){
                System.out.println("Loading attack image "+i);
                attackImages[i] = getImage(getCodeBase(),"ufo_attack"+i+".gif");
                System.out.println("Add ufo attack image "+i);
                t.addImage(attackImages[i],0);
            }
            
            for(int i=0; i<explodeImages.length;i++){
                System.out.println("Loading explode image "+i);
                explodeImages[i] = getImage(getCodeBase(),"ufo_explode"+i+".gif");
                System.out.println("Add ufo explode image "+i);
                t.addImage(explodeImages[i],0);
            }
            
            System.out.println("exit photo loop");
            try{
                System.out.println("Waiting for all");
                t.waitForAll();
                System.out.println("Waiting done!!!");
            }catch(InterruptedException e){System.out.print(">> GameManager.loadImages << InterruptedException:"+e.getMessage());}
            catch(Exception e){System.out.print(">> GameManager.loadImages << exception:"+e.getMessage());}
            
            if(t.isErrorAny()){
                showStatus("Error Loading Images!");
            } else if(t.checkAll()){
                showStatus("Images successfuly loaded");
            } else{
                System.out.println("Unable to load images");
            }
        }catch(Exception e){
            System.out.print(">> GameManager.loadImages << exception:"+e.getMessage());
        }
    }
    
    public void newGame(){
        score = 0;
        numLanded = 0;
        gm.newGame();
        um.newGame();
        sc.setFont(smallFont);
    }
    
    public boolean mouseMove(Event e, int x, int y){
        if(playing) gm.moveGun(x);
        return true;
    }
    
    public boolean mouseDrag(Event e, int x, int y){
        if(playing) gm.moveGun(x);
        return true;
    }
    
    public boolean mouseDown(Event e, int x, int y){
        if(playing) gm.fireMissile(x); 
        else if(screen == INTRO){
            playing = true;
            newGame();
        } else if(screen == GAME_OVER){
            if(e.shiftDown()) screen = INTRO;
        }
        return true;
    }
    
    public void start(){
        showStatus("Stating Game!");
        animation = new Thread(this);
        if(animation !=null){
            animation.start();
        }
    }
    
    public void updateManagers(){
        if(playing) gm.update();
        um.update();
    }
    
    public void update(Graphics g){
        paint(g);
    }
    
    public void paint(Graphics g){
        sc.setColor(Color.black);
        sc.fillRect(0, 0, width, height);
        if(playing){

            drawStatusInfo();

            gm.paint(sc);
            um.paint(sc);
        } else if(screen == INTRO){
            
            sc.setFont(smallFont);
            drawStatusInfo();
            
            um.paint(sc);
            
            sc.setFont(bigFont);
            sc.setColor(Color.green);
            sc.drawString(alienLandingString, (width-stringWidth)/2, height/6);
            
            sc.setColor(Color.magenta);
            sc.setFont(mediumFont);
            
            for(int i =0;i<introString.length-1;i++){
                sc.drawString(introString[i], 13, (3+i)*height/12);
            }
            
            sc.setColor(Color.green);
            sc.drawString(introString[7], (width-stringWidth)/2, height*11/12);
        } else if (screen == GAME_OVER){
            
            sc.setFont(smallFont);
            drawStatusInfo();
            
            gm.paint(sc);
            um.paint(sc);
            
            sc.setFont(bigFont);
            sc.setColor(Color.red);
            sc.drawString(gameOverString, (width-stringWidth)/2, height/2);
            
            sc.setFont(mediumFont);
            sc.setColor(Color.green);
            sc.drawString(clickString, (width-stringWidth-17)/2, height*11/12);
        }
        g.drawImage(image, 0, 0, this);
    }
    
    private void drawStatusInfo(){
        sc.setColor(Color.cyan);
        sc.drawString(scoreString+score, width-113,13);
        sc.drawString(ufoLandedString+numLanded, width-113, 27);
    }

    @Override
    public void run() {
        while(true){
            repaint();
            updateManagers();
            Thread.currentThread().yield();
            try{
                Thread.sleep(REFRESH_RATE);
            }catch(Exception e){}
        }
    }
    
    public void stop(){
        showStatus("Game Stopped");
        if(animation !=null){
            animation.stop();
            animation = null;
        }
    }
    
    public void incrementScore(){
        score +=  UFO_VALUE;
    }
    
    public void alienLanded(){
        numLanded++;
        if(numLanded == MAX_LANDED){
            gameOver();
        }
    }
    
    public void gameOver(){
        if(playing){
            playing = false;
            screen = GAME_OVER;
            f.gameOver();
        }
    }
    
    public void setOptions(int startLevel, int energyDec,boolean sound){
        if(startLevel >=1 && startLevel < MAX_LEVEL){
            this.startLevel = startLevel;
            um.setStartLevel(startLevel);
        }
        if(energyDec >= 0 && energyDec <= MAX_ENERGY){
            this.energyDec = energyDec;
            gm.setEnergyDec(energyDec);
        }
        this.sound = sound;
        um.setSound(sound);
    
    }
    
}
