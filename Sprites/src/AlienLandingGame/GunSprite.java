
package AlienLandingGame;

import java.applet.Applet;
import java.awt.Image;
import sprites.BitmapSprite;
import sprites.Moveable;

public class GunSprite extends BitmapSprite  implements Moveable, Intersect {
    
    protected GunManager gm;
    
    public GunSprite(Image i, Applet a, GunManager gm) {
        super(i, a);
        this.gm = gm;
    }
    
    @Override
    public void setPosition(int x, int y) {
        locx=x;
        locy=y;
    }

    @Override
    public void setVelocity(int x, int y) {
        
    }

    @Override
    public void updatePosition() {
        
    }

    @Override
    public boolean intersect(int x1, int y1, int x2, int y2) {
        return visible && 
                (x2>=locx) && (locx+width>=x1) &&
                (y2>=locy) && (locy+height>=y1);
    }

    @Override
    public void hit() {
        gm.handleHit();
    }
    
}
