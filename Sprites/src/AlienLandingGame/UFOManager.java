
package AlienLandingGame;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Graphics;
import java.awt.Image;
import sprites.Moveable;

public class UFOManager {
    
    
    public UFO ufo[];
    static final int NUM_UFOS = 7;
    
    int ufosKilled;
    int level;
    
    int startLevel;
    int maxLevel;
    boolean playSound = false;
    AudioClip expSound;
    
    static final int KILL_FOR_NEXT_LEVEL=13;
    
    static int width, height;
    
    public UFOManager(int startLevel, int maxLevel,int w, int h, 
            Image ufoImages[], Image[] attackImages, Image[] explodeImages,
            Applet a, AudioClip exp){
        try{
            this.startLevel = startLevel;
            this.maxLevel = maxLevel;
            width = w;
            height = h;

            ufo = new UFO[maxLevel];
            for(int i=0; i<ufo.length;i++){
                //System.out.println("#### creating ufo"+i);
                ufo[i] = new UFO(ufoImages, attackImages, explodeImages,width,height,this, a);
                //System.out.println("#### created ufo"+i);
                //initializePosition(ufo[i]);
            }
            expSound = exp;
            newGame();
        }catch(Exception e){System.out.println(">> UFOManager << exception "+e.getMessage());
            e.printStackTrace();
        }
    }
    
    public UFOManager(int w, int h, Image img, Image ufoImages[],  Applet a){
        try{
            width = w;
            height = h;

            ufo = new UFO[NUM_UFOS];
            for(int i=0; i<ufo.length;i++){
                System.out.println("#### creating ufo"+i);
                ufo[i] = new UFO(ufoImages, width, height,img, a);
                System.out.println("#### created ufo"+i);
                initializePosition(ufo[i]);
            }
        }catch(Exception e){System.out.println(">> UFOManager << exception "+e.getMessage());
            e.printStackTrace();
        }
    }
    
    public void initialize(GunManager gm){
        UFO.initialize(gm);
    }
    
    public void initializePosition(Moveable m){
        m.setPosition(UFO.getRand(width-100)+50,
                    UFO.getRand(height-150)+10);
    }
    
    public void newGame(){
        ufosKilled = 0;
        level =startLevel;
        
        for(int i =0;i<ufo.length;i++){
            initializePosition(ufo[i]);
            if(i >= level){
                ufo[i].suspend();
            }
        }
    }
    
    public UFO[] getUFO(){
        return ufo;
    }
    
    public void paint(Graphics g){
        for(int i=0; i<level;i++){
            ufo[i].paint(g);
        }
    }
    
    public void update(){
        for(int i=0; i<level;i++){
            if(ufo[i].isActive()){
                ufo[i].update();
            } else{
                initializePosition(ufo[i]);
                ufo[i].init();
            }
        }
    }
    
    public void killed(){
        ufosKilled++;
        if(ufosKilled % KILL_FOR_NEXT_LEVEL == 0){
            level = (level == maxLevel) ? maxLevel : level +1;
        }
    }
    
    public void setStartLevel(int start){
        startLevel = start;
    }
    
    public void sound(boolean s){
        playSound = s;
    }
    
    public void playExplosion(){
        if(playSound && expSound !=null){
            expSound.play();
        }
    }

    void setSound(boolean sound) {
        this.sound(sound);
    }
    
}
