
package AlienLandingGame;

import java.awt.Color;
import sprites.RectSprite;

public class MissileSprite extends RectSprite {
    
    protected int vy;
    protected int start_y, stop_y;
    Intersect target[];
    
    public MissileSprite(int w, int h, Color c, int vy, 
                            int start_y, int stop_y, 
                            Intersect target[]) {
        super(w, h, c);
        setFill(true);
        this.vy = vy;
        this.start_y = start_y;
        this.stop_y = stop_y;
        this.target = target;
        suspend();
    }
    
    public void init(int x){
        locx = x;
        locy = start_y;
        restore();
    }
    
    public void update(){
        if(active){
            locy+=vy;
            if(locy<stop_y){
                suspend();
            } else{
                for(int i=0; i<target.length;i++){
                    if(target[i].intersect(locx, locy, locx+width,locy+height)){
                        target[i].hit();
                        suspend();
                        break;
                    }
                }
            }
        }
    }
    
}
