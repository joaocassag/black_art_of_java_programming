
package AlienLandingGame;

import events.BitmapLoop;
import java.applet.Applet;
import java.awt.Image;
//see 6-5
public class UFO extends BitmapLoop implements Intersect {
    
    byte state;
    
    static final byte STANDBY = 0;
    static final byte ATTACK = 1;
    static final byte RETREAT = 2;
    static final byte LAND = 3;
    static final byte EXPLODE = 4;
    
    static final double STANDBY_EXIT = .95;
    static final double ATTACK_EXIT = .95;
    static final double RETREAT_EXIT = .95;
    static final double LAND_EXIT = .95;
    static final double FLIP_X = .9;
    static final int RETREAT_Y = 33;
    
    protected Image ufo[];
    protected Image attack[];
    protected Image explode[];
    
    int max_x, max_y;
    int explosion_counter;
    
    UFOManager um;
    
    static Intersect target;
    static int gun_y;
    static GameManager game;
    
    public UFO(Image ufoImages[],Image attackImages[],Image explodeImages[],
            int max_x, int max_y, 
            UFOManager um, Applet a) {
        super(0, 0, null, ufoImages, a);
        this.max_x = max_x;
        this.max_y = max_y;
        currentImage = getRand(ufoImages.length);
        ufo = ufoImages;
        attack = attackImages;
        explode = explodeImages;
        game = (GameManager) a;
        this.um = um;
        currentImage = getRand(5);
        startStandby();
    }
    
    public UFO(Image ufoImages[],int max_x, int max_y, Image i, Applet a) {
        super(0, 0, i, ufoImages, a);
        this.max_x = max_x;
        this.max_y = max_y;
        currentImage = getRand(5);
        startStandby();
    }
    
    static public void initialize(GunManager gm){
        target = gm.getGun();
        gun_y = gm.getGunY();
    }

    @Override
    public boolean intersect(int x1, int y1, int x2, int y2) {
        return visible && 
                (x2>=locx) && (locx+width>=x1) &&
                (y2>=locy) && (locy+height>=y1);
    }

    @Override
    public void hit() {
        if(state == ATTACK){
            locy = -17;
        } else if(state != EXPLODE){
            startExplode();
            game.incrementScore();
            um.killed();
        }
    }
    
    public void init(){
        startStandby();
        images = ufo;
        restore();
    }
    
    public void update(){
        if((locy+height>=gun_y)&&
           target.intersect(locx, locy, locx+width, locy+height)){
                target.hit();
                if(state !=ATTACK){
                    startExplode();
                    return;
                } else{
                    startRetreat();
                }
        }
        
        double r1 = Math.random();
        double r2 = Math.random();
        switch(state){
            case STANDBY:
                if(r1> STANDBY_EXIT){
                    if(r2> 0.5){
                        startAttack();
                    } else{
                        startLand();
                    }
                } else if((locx<width) || (locx>max_x-width) || (r2 > FLIP_X)){
                    vx = -vx;
                }
                break;
            case ATTACK:
                if((r1>ATTACK_EXIT) || (locy>gun_y-17)){
                    startRetreat();
                }
                else if((locx<width) || (locx>max_x - width) || (r2 > FLIP_X)){
                    vx = -vx;
                }
                break;
            case RETREAT:
                if(r1> RETREAT_EXIT){
                    if(r2>0.5){
                        startAttack();
                    } else{
                        startStandby();
                    }
                }
                else if(locy < RETREAT_Y){
                    startStandby();
                }
                break;
            case LAND:
                if(r1>LAND_EXIT){
                    startStandby();
                }
                else if(locy >=max_y-height){
                    landingRoutine();
                }
                break;
            case EXPLODE:
                explosion_counter++;
                if(explosion_counter == explode.length){
                    suspend();
                }
                break;
        }
        super.update();
    }

    protected void landingRoutine() {
        System.out.println("ufo landed");
        game.alienLanded();
        suspend();
    }
    
    protected void startStandby(){
        vx = getRand(8)-4;
        vy = 0;
        state = STANDBY;
    }
    
    protected void startAttack(){
        vx = getRand(10)-5;
        vy = getRand(5)+4;
        images = attack;
        state = ATTACK;
    }
    
    protected void startRetreat(){
        vx = 0;
        vy = -getRand(3)-2;
        images = ufo;
        state = RETREAT;
    }
    
    protected void startLand(){
        vx = 0;
        vy = getRand(3)+2;
        state = LAND;
    }
    
    protected void startExplode(){
        images = explode;
        currentImage = 0;
        explosion_counter=0;
        um.playExplosion();
        state = EXPLODE;
    }
    
    static public int getRand(int x){
        return (int) (x*Math.random());
    }
    
}
