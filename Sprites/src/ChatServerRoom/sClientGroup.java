
package ChatServerRoom;

import java.net.Socket;
import java.util.StringTokenizer;
import java.util.Vector;

public class sClientGroup extends Thread {
    Vector theGroup;
    
    public sClientGroup(){
        theGroup = new Vector();
    }
    
    public void addClient(Socket s){
        sClientThread tempThread = new sClientThread(s, this);
        theGroup.addElement(tempThread);
        tempThread.start();
    }
    
    public void sendMessage(String msg, String type){
        for(int x =0; x<theGroup.size();x++)
            ((sClientThread)theGroup.elementAt(x)).message(type+"||"+msg);
    }
    
    public void sendMessage(String msg, String target, String type){
        sClientThread tempThread;
        for(int x = 0; x<theGroup.size();x++){
            tempThread = (sClientThread)theGroup.elementAt(x);
            if(tempThread.getAlias().equals(target)){
                tempThread.message(type+"||"+msg);
                //ie. TODO: we can break now, or could there be multiple occurances?
            }
        }
    }
    
    public void handleInput(String str, sClientThread T){
        StringTokenizer st;
        System.out.println("Got: " +str+ " from "+T.getAlias());
        
        if(str.startsWith("bye")){
            T.stop();
            return;
        }
        st = new StringTokenizer(str, "||");
        
        if(st !=null){
            String cmd = st.nextToken();
            String val = st.nextToken();
            if(cmd.equals("login")){
                T.setAlias(val);
                sendMessage(T.getAlias()+"||"+T.getAlias()+" has entered the room.",cmd);
                sendMessage(calcList(), "list");
                return;
            }
            else if(cmd.equals("logout")){
                sendMessage(T.getAlias()+" has left the room.",cmd);
                T.stop();
                return;
            }
            else if(cmd.equals("say")){
                sendMessage(T.getAlias()+" says: "+val,cmd);
                return;
            }
            else if(cmd.equals("whisper")){
                sendMessage(T.getAlias()+" whispers to you: "+val,st.nextToken(),cmd);
                return;
            } else {
                sendMessage(T.getAlias()+" unrecognized: "+val,cmd);
                return;
            }
        
        }
    }
    
    public String calcList(){
        StringBuffer buf = new StringBuffer();
        String temp;
        for(int x = 0; x<theGroup.size();x++){
            temp = ((sClientThread) (theGroup.elementAt(x))).getAlias();
            if(temp != null)
                buf.append(temp).append('&');
        }
        
        if(buf.length()>0)
            buf.setLength(buf.length()-1);
        return buf.toString();
    }
    
    public void cleanHouse(){
        sClientThread tempThread;
        for(int x = 0; x<theGroup.size();x++){
            tempThread = (sClientThread)theGroup.elementAt(x);
            if(tempThread == null || !tempThread.isAlive()){
                theGroup.removeElement(tempThread);
            }
        }
    }
    
    public void run(){
        while(true){
            try{sleep(30000);}catch(Exception e){}
            cleanHouse();
        }
    }
}
