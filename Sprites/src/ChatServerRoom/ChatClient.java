
package ChatServerRoom;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.io.DataInputStream;
import java.io.PrintStream;
import static java.lang.Thread.sleep;
import java.net.Socket;
import java.util.StringTokenizer;

public class ChatClient extends Applet implements Runnable {
    
    Socket sock;
    DataInputStream dis;
    PrintStream ps;
    String name, theHost;
    int thePort;
    
    Thread kicker = null;
    
    TextField inputField;
    TextArea outputArea;
    Button B1, B2;
    List L;
    Panel p1, p2;
    
    public void init(){
        setLayout(new BorderLayout());
        
        p1 = new Panel();
        p2 = new Panel();
        
        p1.setLayout(new FlowLayout());
        
        add("South",p1);
        add("North",p2);
        
        inputField = new TextField(80);
        p1.add(inputField);
        
        outputArea = new TextArea(10,60);
        p2.add(outputArea);
        
        outputArea.setEditable(false);
        outputArea.setBackground(Color.cyan);
        
        B1 = new Button("login");
        p1.add(B1);
        
        B2 = new Button("logout");
        p1.add(B2);
        B2.setEnabled(false);
        
        L = new List();
        p2.add(L);
        L.add("All Participants");
        
        L.select(0);
    }
    
    public boolean output(String str){
        try{
            ps.println(str);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
    
    public boolean handleEvent(Event evt){
        if(evt != null){
            if(evt.id == Event.LIST_DESELECT){
                L.select(0);
                return true;
            }
            
            if(evt.id == Event.LIST_SELECT){
                if(L.getSelectedIndex() == 0)
                    B1.setLabel("say");
                else
                    B1.setLabel("whisper");
                p1.doLayout();
                return true;
            }
            
            if(evt.target.equals(inputField) && evt.id ==Event.ACTION_EVENT)
                evt.arg = B1.getLabel();
            
            if(evt.arg != null){
                if(evt.arg.equals("login")){
                    outputArea.append("Logging in ...\n");
                    B1.setLabel("say");
                    B2.setEnabled(true);
                    name = inputField.getText();
                    inputField.setText("");
                    
                    kicker = new Thread(this);
                    kicker.start();
                }
                if(evt.arg.equals("say")){
                    output("say||"+inputField.getText());
                    inputField.selectAll();
                    return true;
                }
                if(evt.arg.equals("whisper")){
                    outputArea.append("You whisper to "+L.getSelectedItem()+": "+inputField.getText()+"\n");
                    output("whisper||"+inputField.getTextListeners()+"||"+L.getSelectedItem());
                    inputField.selectAll();
                    return true;
                }
            }
            if(evt.target.equals(B2)){
                    outputArea.append("Logging out ...\n");
                    kicker.stop();
                    B2.setEnabled(false);
                    B1.setLabel("login");
                    p1.doLayout();
                    return true;
                }
        }
        return super.handleEvent(evt);
    }

    @Override
    public void run() {
        while(sock ==null && kicker !=null){
            try{
                sock = new Socket(theHost,thePort);
                dis = new DataInputStream(sock.getInputStream());
                ps = new PrintStream(sock.getOutputStream());
            }catch(Exception e){
                System.out.println("Unable to contact host.");
                outputArea.append("Unable to contact host. Retrying...\n");
                sock = null;
            }
            try{sleep(5000);}catch(Exception e){}
        }
        
        output("login||"+name);
        outputArea.append("Logged in to server successfully.\n");
        
        //handle input from the server
        while(sock != null && dis !=null && kicker !=null){
            try{
                String str = dis.readLine();
                System.out.println("Got: "+str);
                if(str!=null)
                    if(str.indexOf("||")!=-1){
                        StringTokenizer st = new StringTokenizer(str,"||");
                        String cmd = st.nextToken();
                        String val = st.nextToken();
                        if(cmd.equals("list")){
                            p2.remove(L);
                            L = new List();
                            p2.add(L);
                            p2.repaint();
                            p2.doLayout();
                            L.add("All Participants");
                            StringTokenizer st2 = new StringTokenizer(val,"&");
                            while(st2.hasMoreTokens())
                                L.add(st2.nextToken(val));
                            L.select(0);
                        }
                        else if(cmd.equals("logout")){
                            for(int x = 0; x< L.getRows();x++)
                                if(val.startsWith(L.getItem(x)))
                                    L.remove(x);
                            outputArea.append(val+"\n");
                            validate();
                        }
                        else if(cmd.equals("login")){
                            outputArea.append(st.nextToken()+"\n");
                        }
                        else
                            outputArea.append(val+"\n");
                    } 
                    else
                        outputArea.append(str+"\n");
            }catch(Exception e){
                System.out.println("Connection lost.");
                kicker.stop();
            }
        }
    }
    
    public void stop(){
        output("logout||"+name);
        try{
            dis.close();
            ps.close();
            sock.close();
        }catch(Exception e){}
        sock = null;
        outputArea.append("Logged out from server.\n");
        kicker = null;
    }
    
}
