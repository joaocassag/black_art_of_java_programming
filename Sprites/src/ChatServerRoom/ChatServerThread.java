
package ChatServerRoom;

import java.net.ServerSocket;
import java.net.Socket;

public class ChatServerThread extends Thread {
    
    ServerSocket servSock = null;
    sClientGroup group;
    
    public ChatServerThread(){
        try{
            servSock = new ServerSocket(1123);
        }catch(Exception e){
            System.out.println("Could not initialize. Exiting.");
            System.exit(1);
        }
        System.out.println("Server successfully initialized. Waiting for connection ...");
        group = new sClientGroup();
        group.start();
    }
    
    public void run(){
        while(servSock != null){
            Socket tempSock;
            try{
                tempSock = servSock.accept();
                System.out.println("Received new connection.");
                group.addClient(tempSock);
            }catch(Exception e){
                System.out.println("New Connectiong Failure. Exiting.");
                System.exit(1);
            }
        }
    }
    
    public void finalize(){
        try{
            servSock.close();
        }catch(Exception e){ servSock = null;}
    }
    
}
