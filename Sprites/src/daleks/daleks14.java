
package daleks;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Event;
import java.awt.Font;
import java.awt.Graphics;

public class daleks14 extends Applet{/* implements Runnable {
    
    public void init(){
        getHtmlParameters();
        
        //initialize the dalek arrays
        dalX = new int[maxEnemies];
        dalY = new int[maxEnemies];
        dalF = new int[maxEnemies];
        
        dirStartX = maxX/2;
        dirStartY = maxY/2;
        
        //create enough room to fit gamboard and status bar
        resize(maxX*imgW,maxY*imgH+25);
        
        //load game gaphics
        doctorPic[0] = getImage(getCodeBase(),"dw1l.gif");
        doctorPic[1] = getImage(getCodeBase(),"dw1r.gif");
        doctorPic[2] = getImage(getCodeBase(),"dw2l.gif");
        doctorPic[3] = doctorPic[2];
        doctorPic[4] = getImage(getCodeBase(),"dw3l.gif");
        doctorPic[5] = doctorPic[4];
        doctorPic[6] = getImage(getCodeBase(),"dw4l.gif");
        doctorPic[7] = getImage(getCodeBase(),"dw4r.gif");
        doctorPic[8] = getImage(getCodeBase(),"dw5l.gif");
        doctorPic[9] = getImage(getCodeBase(),"dw4r.gif");
        
        dalekPic[0] = getImage(getCodeBase(),"dalekl.gif");
        dalekPic[1] = getImage(getCodeBase(),"dalekr.gif");
        
        rubblePic[0] = getImage(getCodeBase(),"daldl.gif");
        rubblePic[1] = getImage(getCodeBase(),"daldr.gif");
        
        deadPic = getImage(getCodeBase(),"drdead.gif");
        
        //load graphics
        for(int j=0;j<10;j++)
            digit[j] = getImage(getCodeBase(),"green"+j+".gif");
        
        for(int j=0;j<10;j++){
            tracker.addImage(doctorPic[j],j);
            tracker.addImage(digit[j],j+10);
        }
        
        for(int j=0;j<2;j++){
            tracker.addImage(dalekPic[j],20+j);
            tracker.addImage(rubblePic[j],22+j);
        }
        tracker.addImage(deadPic,24);
        
        //start loading images
        tracker.checkAll(true);
        
        //create the off-screen bitmap
        nextScreen = createImage(maxX*imgW,maxY*imgH+25);
        offScreenGC = nextScreen.getGraphics();
        
        preapreScreen();
        
        animateIntro.setInfo(nextScreen,maxX,maxY,imgH,imgW,title,flyingWidget,numWidgets);
        animateIntro.start();
        animIntro = true;
        repaint();
    }
    
    public void start(){
        if(kicker ==null){
            kicker = new Thread(this);
            kicker.start();
        }
        repaint();
    }
    
    public synchronized void update(Graphics g){
        //draw between-move animations and repaint the game board, or
        //call the intro screen's drawing routine
        repoaintDone = false;
        if(animIntro){
            animateIntro.drawIntroScreen(g,doneLoading,currentPic);
            if(!doneLoading){
                if(tracker.checkID(currentPic,true));
                    currentPic++;
                if(currentPic > 24){
                    doneLoading = true;
                    prepareSreen();
                }
                repaint();
            }
            else{
                //TODO:
            }
        }
    }
    
    public synchronized boolean keyDown(Event evt, int x){
        if(doneLoading)
            handleKeyPress(x);
        return true;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void handleKeyPress(int whichKey){
        if(animIntro){
            //stop the intro screen and begin the game
            //TODO:
        } else if(levelComplete){
            //set up for the next level
            //TODO:
        }else if(playerDead){
            //animate the intro screen
            //TODO:
        }else{
            //analyze keypress and execute the next turn
            char ASCIImove = (char) whichKey;
            boolean validMove = movePlayer(ASCIImove);
            if(validMove){
                if(lastStand)
                    repaint();
                else{
                    if(screwdriverUsed){
                        calcSonic();
                        play(getCodeBase(),"sonic.au");
                    }
                    moveDaleks();
                    if(newRubble)
                        play(getCodeBase(),"crash.au");
                }
            }
            repaint();
            checkDaleks();
            if((drA) && (allDead))
                levelComplete = true;
            else if(!drA){
                prepareScreen();
                repaint();
                playerDead = true;
            }
        }
    }
    
    public boolean movePlayer(char input){
        int newX = drX;
        int newY = drY;
        int newF = drF;
        boolean pass =false;
        //player may elect to not move (hit '5')
        boolean teleport = false;
        //player may teleport (hit 'T' or 't')
        boolean validMove = true;
        switch(input){
            //numeric keypad movement options
            case '1': newX--; newY++; newF = faceLeft; break;
            case '2':         newY++;                    break;
            case '3': newX++; newY++; newF = faceRight;  break;
            case '4': newX--;         newF = faceLeft;   break;
            case '5': pass=true;                         break;
            case '6': newX++;         newF = faceRight;  break;
            case '7': newX--; newY--; newF = faceLeft;   break;
            case '8':         newY--;                    break;
            case '9': newX++; newY--; newF = faceRight;  break;
            
            //alternative keus movement
            case 'N': 
            case 'n': newX--; newY++; newF = faceLeft;  break;
            case 'M':
            case 'm':         newY++;                   break;
            case ',': newX++; newY++; newF = faceRight;  break;
            case 'H':
            case 'h': newX--;          newF = faceLeft;  break;
            case 'J':
            case 'j': pass=true;                         break;
            case 'K':
            case 'k': newX++;          newF = faceRight;  break;
            case 'Y':
            case 'y': newX--; newY--; newF = faceLeft;  break;
            case 'U':
            case 'u':         newY--;                   break;
            case 'I':
            case 'i': newX++; newY--; newF = faceRight;  break;
            
            //other valid commands
            case 'T':
            case 't': pass=true;                         break;
            case 'S':
            case 's': screwdriverUser=true;              break;
            case 'L':
            case 'l': lastStand=true;                    break;
            default: validMove=false;
        }
        
        //chek if move is out of bounds
        if((newX<0) || (newX>maxX-1 || (newY<0) || (newY>maxY-1)
                validMove = false;
        
        //find an empty locaiton if teleporting
        if(teleport){
            boolean okLoc = false;
            //loop until valid coordinates are found
            while(!okLoc){
                okLoc = true;
                newX = (int)(Math.random()*maxX);
                newY = (int)(Math.random()*maxY);
                for(int j=0;j<numDaleks;j++)
                    if(((newX==dalX[j]) && (newY==dalY[j])) &&(dalA[j])||(dalR[j]))
                        okLoc = false;
            }
            animTele = true;
        }
        //check if the player is allowrd to use the screwdriver
        if(screwdriverUser && (screwdrivers ==0)){
            validMove = false;
            srewdriverUsed = false;
        }

        //check ig attempting to move to a location contatining rubble
        for(int j =0;j<numDaleks;j++)
            if((newX=dalX[j]) && (newY==dalY[j]) && (dalR[j]))
                validMove=false;

        //if verything is ok, record new position & orientation
        if(validMove){
            drX = newX;
            drY = nexY;
            drF = newF;
        }
        return validMove;
    }
    
    public void moveDaleks(){
        //move each dalek towark player
        for(int j=0;j<numDaleks;j++)
            //only move if dalek is alive
            if(dalA[j]){
                if(dalX[j]<drX) dalX[j]++;
                if(dalY[j]<drY) dalY[j]++;
                if(dalX[j]>drX) dalX[j]--;
                if(dalY[j]>drY) dalY[j]--;
                if((dalX[j]==drX) && (dalY[j]==drY)) drA = false; //player has died
            }
            
        //check colisions between dalkes 
        newRubble = false;
        for(int j=0;j<numDalkes;j++){
            for(int k=0;k<numDalkes;k++)
                if((dalX[j]==dalX[k]) && (dalY[j]==dalY[k]) && 
                        ((dalA[j]) || (dalR[j])) && ((dalA[k]) || (dalR[k])) ){
                    if(dalA[j]){
                        score += level*2;
                        newRubble=true;
                    }
                    if(dalA[k]){
                        score += level*2;
                        newRubble=true;
                    }
                    dalA[j] = dalA[k] = false;// both daleks are dead
                    dalR[j] = dalR[k] = true;// both daleks are rubble\
                }
        }
    }
    
    public void animateTele(Graphics g){
        //animate pairs of gray lines when teleporting
        g.clipRect(0,0, maxX*imgW, maxY*imgH);
        g.setColor(Color.lightGray);
        for(int j=maxX*imgW+8;j>8;j-=8){
            g.drawLine(drX*imgW-j, drY*imgH-j,drX*imgW+j, drY*imgH-j);
            g.drawLine(drX*imgW-j, drY*imgH+j,drX*imgW+j, drY*imgH+j);
            pause(800);
        }
        animTele = false;
    
    }

    public synchronized void prepareSreen() {
        //draw the current game status on the off screen bitmap
        offScreenGC.setColor(Color.white);
        offScreenGC.clipRect(0,0,maxX*imgW,maxY*imgH+25);
        offScreenGC.fillRect(0,0,maxX*imgW,maxY*imgH+4);
        
        //draw info bar on bottom
        offScreenGC.setColor(Color.darkGray);
        offScreenGC.drawLine(0,maxY*imgH+1,maxX*imgW,maxY*imgH+1);
        offScreenGC.drawLine(0,maxY*imgH+3,maxX*imgW,maxY*imgH+3);
        offScreenGC.setColor(Color.lightGray);
        offScreenGC.drawLine(0,maxY*imgH+2,maxX*imgW,maxY*imgH+2);
        offScreenGC.setColor(Color.black);
        offScreenGC.fillRect(0,maxY*imgH+4,maxX*imgW,21);
        if(doneLoading){
            offScreenGC.setFont(new Font("TimeRoman",Font.PLAIN,12));
            if(score>highScore)
                highScore = score;
            int yPos = maxY*imgH+19;
            offScreeenGC.setColor(Color.white);
            offScreenGC.drawString("SCORE: ",5,yPos);
            paintNumber(score,51,yPos);
            
            offScreenGC.drawString("SCREWDRIVERS: ",124,yPos);
            paintNumber(screwdrivers,221,yPos);
            
            offScreenGC.drawString("LEVEL: ",260,yPos);
            paintNumber(level,303,yPos);
            
            offScreenGC.drawString("HIGHSCORE: ",338,yPos);
            paintNumber(highscore,415,yPos);
            
            //draw the daleks
            for(int j=0;j<numDaleks;j++){
                if(dalA[j]){
                    checkFace(j);
                    offScreenGC.drawImage(dalekPic[dalF[j]],dalX[j]*imgW,dalY[j]*imgH,this);
                } else if(dalR[j])
                    offScreenGC.drawImage(rubblePic[dalF[j]],dalX[j]*imgW,dalY[j]*imgH,this);
            }
            
            //draw the doctor
            if(!playerDead)
                offScreenGC.drawImage(doctorPic[drF+whichDoc],drX*imgW,drY*imgH,this);
            else{
                offScreenGC.setColor(Color.white);
                offScreenGC.fillRect(drX*imgW,drY*imgH,imgW,imgH);
                offScreenGC.drawImage(deadPic,drX*imgW,drY*imgH,this);
                
                offScreenGC.setColor(Color.black);
                offScreenGC.setFont(new Font("Helvetica",Font.PLAIN,36));
                offScreenGC.drawString("GAME OVER: ",maxX*imgW/2-100,75);
            }
        }
    }
    
    public void paintNumber(int num, int xPOs, int yPos){
        //draw the number as a graphic at the specified position
        Integer numObj = new Integer(num);
        String stringNum = numObj.toString();
        for(int j=0;j<stringNum.length();j++){
            char charDigit = stringNum.charAt(j);
            int intDigit = Character.digit(charDigit, 10);
            offScreenGC.drawImage(digit[intDigit],xPos+j*12,yPos-13,this);
        }
    }
    
    public void checkDaleks(){
        //evaluate wheter all the daleks are dead and set the 'allDead' variable accordingly
        allDead = true;
        for(int j=0;j<numDaleks;j++)
            if(dalA[j])
                allDead = false;
    }

    public void getHtmlParameters() {
        //TODO:
    }
*/
}
