package broadway;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class Broadway extends Applet implements Runnable {
    
    Thread animation;
    int locx,locy;
    int width, height;
    static final byte UP=0;
    static final byte DOWN=1;
    static final byte LEFT=2;
    static final byte RIGHT=3;
    byte state;
    static final int REFRESH_RATE = 100;
    
    //Graphics offscreen;
    //Image image;
    
    public void init() {
        System.out.println(">> init <<");
        setBackground(Color.black);
        locx = 80;
        locy = 100;
        width = 110;
        height = 90;
        state = UP;
        //image = createImage(width,height);
        //offscreen = image.getGraphics();
    }
    
    public void start(){
        System.out.println(">> start <<");
        animation = new Thread(this);
        if(animation != null){
            animation.start();
        }
    }
    
    public void paint(Graphics g){
        System.out.println(">> paint <<");
        
        g.setColor(Color.yellow);
        g.fillRect(0, 0, 90, 90);
        g.fillRect(250, 0, 40, 190);
        g.fillRect(80, 110, 100, 20);
        
        g.setColor(Color.blue);
        g.fillRect(80, 200, 220, 90);
        g.fillRect(100, 10, 90, 80);
        
        g.setColor(Color.lightGray);
        g.fillRect(locx, locy, width, height);
        g.fillRect(0, 100, 70, 200);
        
        g.setColor(Color.red);
        g.fillRect(200, 0, 45, 45);
        g.fillRect(0, 100, 70, 200);
        
        g.setColor(Color.magenta);
        g.fillRect(200, 55, 60, 135);
        
        /*
        offscreen.setColor(Color.black);
        offscreen.fillRect(0, 0, 300, 300);
        
        offscreen.setColor(Color.yellow);
        offscreen.fillRect(0, 0, 90, 90);
        offscreen.fillRect(250, 0, 40, 190);
        offscreen.fillRect(80, 110, 100, 20);
        
        offscreen.setColor(Color.blue);
        offscreen.fillRect(80, 200, 220, 90);
        offscreen.fillRect(100, 10, 90, 80);
        
        offscreen.setColor(Color.lightGray);
        offscreen.fillRect(locx, locy, width, height);
        offscreen.fillRect(0, 100, 70, 200);
        
        offscreen.setColor(Color.red);
        offscreen.fillRect(200, 0, 45, 45);
        offscreen.fillRect(0, 100, 70, 200);
        
        offscreen.setColor(Color.magenta);
        offscreen.fillRect(200, 55, 60, 135);
        g.drawImage(image, 0, 0, this);
        */
        
    }
    
    /*public void update(Graphics g){
        //g.clipRect(50, 50, 130, 110);
        paint(g);
    }*/
    
    void updateRectangle(){
        switch(state){
            case DOWN:
                locy+=2;
                if(locy>=110)state = UP;
                break;
            case UP:
                locy-=2;
                if(locy<=90) state = RIGHT;
                break;
            case RIGHT:
                locx+=2;
                if(locx>=90) state = LEFT;
                break;
            case LEFT:
                locx-=2;
                if(locx<=70) state = DOWN;
                break;
        }
    }
    
    public void stop(){
        System.out.println(">> stop <<");
        if(animation !=null){
            animation.stop();
            animation = null;
        }
    }
    
    public void destroy(){
        System.out.println(">> destroy <<");
    }

    @Override
    public void run() {
       while(true){
           repaint();
           //repaint(70,90,130,110);
           updateRectangle();
           try{ Thread.sleep(REFRESH_RATE);
           }catch(Exception e){}
       }
    }
}

