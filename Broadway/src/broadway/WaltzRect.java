
package broadway;

import java.awt.Color;

public class WaltzRect extends DancingRect {
    
    static final byte SE=0;
    static final byte NE=1;
    static final byte W=2;
    static final byte RIGHT=3;
    
    byte state;
    
    int bottom_x;
    int right_x;
    int left_x;
    
    public WaltzRect(int x, int y, int w, int h, Color c){
        super(x,y,w,h,c);
        bottom_x= locx + 17;
        right_x = bottom_x + 13;
        left_x = locx;
    }
    
    public void danceStep(){
        switch(state){
            case SE:
                locx++;
                locy++;
                if(locx==bottom_x)state = NE;
                break;
            case NE:
                locx++;
                locy--;
                if(locy==right_x) state = W;
                break;
            case W:
                locx--;
                if(locx>=left_x) state = SE;
                break;
        }
    }
    
}
