package broadway;

import java.awt.Color;
import java.awt.Graphics;

public class DancingRect{
    int locx, locy;
    int width, height;
    Color myColor;
    
    public DancingRect(int locx, int locy, int width, int height, Color myColor){
        this.locx = locx;
        this.locy = locy;
        this.width = width;
        this.height = height;
        this.myColor = myColor;
    }
    
    public void danceStep(){
    
    }
    
    public void paint(Graphics g){
        g.setColor(myColor);
        g.fillRect(locx, locy, width, height);
    }
}