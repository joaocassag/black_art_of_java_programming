
package broadway;

import java.awt.Color;

public class BoogieRect extends DancingRect {
    
    static final byte UP=0;
    static final byte DOWN=1;
    static final byte LEFT=2;
    static final byte RIGHT=3;
    
    byte state;
    
    int max_x,min_x;
    int max_y, min_y;
    
    public BoogieRect(int x, int y, int w, int h, Color c){
        super(x,y,w,h,c);
        max_x = locx + 13;
        min_x = locx - 13;
        max_y = locy + 13;
        min_y = locy - 13;
    }
    
    public void danceStep(){
        switch(state){
            case DOWN:
                locy+=2;
                if(locy>=max_y)state = UP;
                break;
            case UP:
                locy-=2;
                if(locy<=min_y) state = RIGHT;
                break;
            case RIGHT:
                locx+=2;
                if(locx>=max_x) state = LEFT;
                break;
            case LEFT:
                locx-=2;
                if(locx<=min_x) state = DOWN;
                break;
        }
    }
    
}
