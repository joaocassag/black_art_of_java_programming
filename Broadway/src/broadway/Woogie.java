
package broadway;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class Woogie extends Applet implements Runnable {
    
    Thread animation;
    Graphics offscreen;
    Image image;
    
    static final int NUM_RECTS = 9;
    DancingRect r[];
    static final int REFRESH_RATE = 100;
    
    public void init() {
        System.out.println(">> init <<");
        setBackground(Color.black);
        initRectangles();
        image = createImage(300,300);
        offscreen = image.getGraphics();
    }
    
    public void initRectangles(){
        r = new DancingRect[NUM_RECTS];
        r[0] = new BoogieRect(0, 0, 90, 90, Color.yellow);
        r[1] = new BoogieRect(250, 0, 40, 190, Color.yellow);
        r[2] = new WaltzRect(80, 110, 100, 20, Color.yellow);
        r[3] = new BoogieRect(80, 200, 220, 90, Color.blue);
        r[4] = new WaltzRect(100, 10, 90, 80, Color.blue);
        r[5] = new BoogieRect(80, 100, 110, 90, Color.lightGray);
        r[6] = new WaltzRect(200, 0, 45, 45, Color.red);
        r[7] = new WaltzRect(0, 100, 70, 200, Color.red);
        r[8] = new BoogieRect(200, 55, 60, 135, Color.magenta);
    }
    
    public void start(){
        System.out.println(">> start <<");
        animation = new Thread(this);
        if(animation != null){
            animation.start();
        }
    }
    
    public void updateRectangles(){
        for(int i=0;i<NUM_RECTS;i++){
            r[i].danceStep();
        }
    }
    
    /*public void update(Graphics g){
        //g.clipRect(50, 50, 130, 110);
        paint(g);
    }*/
    
    public void paint(Graphics g){
        offscreen.setColor(Color.black);
        offscreen.fillRect(0, 0, 300, 300);
        for(int i=0;i<NUM_RECTS;i++){
            r[i].paint(offscreen);
        }
        g.drawImage(image, 0, 0, this);
    }
    
    public void stop(){
        System.out.println(">> stop <<");
        if(animation !=null){
            animation.stop();
            animation = null;
        }
    }
    
    public void destroy(){
        System.out.println(">> destroy <<");
    }

    @Override
    public void run() {
       while(true){
           repaint();
           //repaint(70,90,130,110);
           updateRectangles();
           try{ Thread.sleep(REFRESH_RATE);
           }catch(Exception e){}
       }
    }
    
}
